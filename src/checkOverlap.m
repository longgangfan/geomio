function checkOverlap(Volume,volLabel,opt)

    % check orientation
    if find(opt.normalDir > 0) == 1
        axes = [2 3];
    elseif find(opt.normalDir > 0) == 2
        axes = [1 3];
    elseif find(opt.normalDir > 0) == 3
        axes = [1 2];
    end
    
    % number of polygons
    numPoly     = length(Volume.Polygons);

    % loop through all polygons but the last one
    for iPoly = 1 : numPoly-1
        sameSlice = 0;

        % loop through all following Polygons
        for iiPoly = iPoly + 1 : numPoly
            % count how many following Polygons are on the same slice
            if Volume.PolyPos(iiPoly) == Volume.PolyPos(iPoly)
                sameSlice = sameSlice + 1;
            else
                break;
            end
        end
                
        if sameSlice
            Polygon = Volume.Polygons{iPoly};
            % loop through polygons on same slice and check for an overlap
            for iiPoly = 1 : sameSlice
                Poly2 = Volume.Polygons{iPoly ++ iiPoly};
                % is Polygon 2 inside of Polygon 1
                if inpolygon(Poly2(1,axes(1)),Poly2(1,axes(2)),Polygon(:,axes(1)),Polygon(:,axes(2)))
                    error(['Overlapping Polygons in ' volLabel '. Try cutting in a different direction (change opt.normalDir in run_geomIO.m)'])
                end
                % is Polygon 1 inside of Polygon 2
                if inpolygon(Poly2(1,axes(1)),Poly2(1,axes(2)),Polygon(:,axes(1)),Polygon(:,axes(2)))
                    error(['Overlapping Polygons in ' volLabel '. Try cutting in a different direction (change opt.normalDir in run_geomIO.m)'])
                end
            end
        end
    end

end