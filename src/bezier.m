function [X] = bezier(CP,nt)
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   bezier.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

if size(CP,2)~=2
    error('Contour points should be given in a Nx2 format')
end

n = size(CP,1)-1;
% nt = size(CP,1); % aribtrary
t = linspace(0,1,nt)';

fn = factorial(n);
% Build order n matrix
B = zeros(nt,n+1);
fk = 1;
fn_k = 1;
fn_k = factorial(n);
for k = 0:n
    B(:,k+1)= fn/ (fk * fn_k) * t.^k .* (1-t).^(n-k);        
    fk = fk*(k+1);
    if k<n    
        fn_k = fn_k/(n-k);
    end
end
X = B*CP;