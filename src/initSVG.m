function initSVG( opt )
% initSVG( opt )
% opt.inputFileName refers to a blank svg file created with inkscape
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   initSVG.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

docNode     = xmlread(opt.inputFileName);
root        = docNode.getDocumentElement;  

svgList     = docNode.getElementsByTagName('svg');
currentSVG  = svgList.item(0);
currentSVG.setIdAttribute('id', true)           % must be defined to use .getElementById, later on

if ~isempty(opt.svgHeight)
    currentSVG.setAttribute('height', sprintf('%i',opt.svgHeight)) 
    currentSVG.setAttribute('width' , sprintf('%i',opt.svgWidth)) 
end

% Get the list of g nodes (layers)
layerList = docNode.getElementsByTagName('g');
noLayers = layerList.getLength;
pathList = docNode.getElementsByTagName('path');
noPaths  = pathList.getLength;

% Is it blank?
if noLayers>1 || noLayers == 0
    error([opt.inputFileName ' should have only ONE blank layer']);
elseif noPaths>0
    error([opt.inputFileName ' is not a blank document']);
end


% make a copy of first layer
cpylayer = docNode.getElementsByTagName('g').item(0).cloneNode(true);

% remove all layers from document
root.removeChild(layerList.item(0));

% --- create layer with linked images
noProf = length(opt.(opt.orient).z);
for il = 1:noProf


    if opt.(opt.orient).z(il) < 0
        layerLabel = sprintf('%s_m%.3d',opt.orient,abs(opt.(opt.orient).z(il)));
        layerLabelOut = sprintf('%s_m%.3d',opt.orient,abs(opt.(opt.orient).z(il)));
    else
        layerLabel = sprintf('%s_p%.3d',opt.orient,abs(opt.(opt.orient).z(il)));
        layerLabelOut = sprintf('%s_%.3d',opt.orient,abs(opt.(opt.orient).z(il)));
    end

%    layerLabel = sprintf('%s_%.3d',opt.orient,opt.(opt.orient).z(il));
    % create clone for current layer 
    thislayer = cpylayer.cloneNode(true);
    
    % change layer attributes
    thislayer.setAttribute('id',sprintf('ilayer%.3d',il));
    thislayer.setAttribute('inkscape:label',layerLabel);
    
    if opt.imgAdd
        if il==1
            % define xlink    
            svgList.item(0).setAttribute('xmlns:xlink','http://www.w3.org/1999/xlink');
        else
            % shows only the first image
            thislayer.setAttribute('style','display:none')
        end
%        ImageInfo = imfinfo([opt.inputDir opt.imgDir,layerLabel,'.', opt.imgType]);
% as we defined imgdir and check whether imdir is absolute path or relative
% path
        ImageInfo = imfinfo([opt.imgDir,layerLabelOut,'.', opt.imgType]);
        % add image
        thisimage = docNode.createElement('image');
        thisimage.setAttribute('id',sprintf('ilayer_im_%.3d',il));
        thisimage.setAttribute('width',sprintf('%.5gpx',ImageInfo.Width));
        thisimage.setAttribute('height',sprintf('%.5gpx',ImageInfo.Height));
        thisimage.setAttribute('x',sprintf('%.5g',opt.imgX));
        thisimage.setAttribute('y',sprintf('%.5g',opt.imgY));
        thisimage.setAttribute('xlink:href',sprintf('file://%s%s.%s',opt.imgDir,layerLabelOut,opt.imgType));
        thisimage.setAttribute('sodipodi:absref',sprintf('%s%s.%s',opt.imgDir,layerLabelOut,opt.imgType));
        thisimage.setAttribute('style','image-rendering:optimizeSpeed');
        thisimage.setAttribute('preserveAspectRatio','none');        


        thislayer.appendChild(thisimage);
    end
    
    root.appendChild(thislayer);
end


% --- add Reference layer and Coordref ---

% create clone for current layer 
thislayer = cpylayer.cloneNode(true);

% change layer attributes
thislayer.setAttribute('id','iref000');
thislayer.setAttribute('inkscape:label','Reference');
thispath = docNode.createElement('path');
thispath.setAttribute('style','fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1');
thispath.setAttribute('d','M 302.85714,2180 3014.2857,408.57143');
thispath.setAttribute('inkscape:connector-curvature','0');
thispath.setAttribute('CoordRef','x1,y1,x2,y2');
thislayer.appendChild(thispath);
root.appendChild(thislayer);




xmlwrite(opt.inputFileName,docNode);

% S = xmlwrite(docNode)

disp(['Initialized svg file: ' opt.inputFileName]);

end

