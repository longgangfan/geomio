function [ pathData ] = writePathData( Commands, InputCoord )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   writePathData.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
C = regexprep(Commands.String,'(\<\w+)','${upper($1)}'); % Capitalizes all commands to write in absolute coordinate
SL = Commands.SectionLength;
pathData = [];
Count = 0;
for iC = 1:length(C)
    cStr = [];
    for iS = 1:SL(iC)
        Count = Count+1;
        cStr = [cStr ' ' num2str(InputCoord(1,Count)) ',' num2str(InputCoord(2,Count))];
    end
    pathData = [pathData C(iC) cStr ' '];
end

end

