function opt = checkOpts(opt)
%
% Check all input options
% 
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   checkOpts.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
fprintf('-----------------------------------------------------------------------------------\n');
fprintf('                                   geomIO\n\n');

% properly linked to installation ?
% 
% PWD = pwd;
% [Repo] = getGeomIORepoAdress()
% cd(Repo);
% system('git log -1 --abbrev-commit > ShortGitLog.txt');
% fid = fopen('ShortGitLog.txt');
% A = fscanf(fid,'%c');
% CommitInfo = regexp(A,'.+\n\n','match');
% fprintf('git status:\n')
% fprintf(CommitInfo{1})
% fclose(fid);
% 
% cd(PWD);

fprintf(['--- Job type: ' opt.runJob '\n\n']);

% debug mode ?
if opt.debug
    fprintf('--- Debug mode \n\n');
end
fprintf('-----------------------------------------------------------------------------------\n\n');

disp('=== I/O paths:');
% make sure the path strings have correct format
opt.inputDir  = cleanString(opt.inputDir);
opt.outputDir = cleanString(opt.outputDir);
opt.imgDir    = cleanString(opt.imgDir);



% input directory and files
if isdir(opt.inputDir)

%     if isfield(opt,'multiSVGs')
%         
%         % if this field doesn't exist we create it
%         if ~isfield(opt,'readMultiSVG')
%             opt.readMultiSVGs = true;
%         end
%     
%         for k = 1:length(opt.multiSVGs)
%             opt.multiSVGs{k} = [opt.inputDir opt.multiSVGs{k}];
% 
%             if exist(opt.multiSVGs{k},'file') == 2
%                 disp(['> Multiple SVG input files: ' opt.multiSVGs{k}])
%             else
%                 error(['Your input file does not exist:' opt.multiSVGs{k}])
%             end
%         end
% 
%         opt.inputFileName = opt.multiSVGs{1};
% 
%     else
        opt.inputFileName      = [opt.inputDir opt.inputFileName];
        if opt.useSVG
            if exist(opt.inputFileName,'file') == 2
                disp(['> Single input file: ' opt.inputFileName])
            else
                error(['Your input file does not exist:' opt.inputFileName])
            end
        end
%         opt.multiSVGs{1}= 0;
%         opt.readMultiSVGs = false;
%     end  

%     if isfield(opt,'orientVert')
%         opt.inputFileNameVert = [opt.inputDir opt.inputFileNameVert];
%         opt.outputFileNameVert = [opt.outputDir opt.outputFileNameVert];
%         if exist(opt.inputFileNameVert,'file') == 2
%             disp(['> Input file for vertical cross section : ' opt.inputFileNameVert])
%         else
%             error(['Your input file does not exist:' opt.inputFileNameVert])
%         end
%     end

    if opt.readLaMEM
        opt.LaMEMinputFileName = [opt.inputDir opt.LaMEMinputFileName];
        if exist(opt.LaMEMinputFileName,'file') == 2
            disp(['> LaMEM input file: ' opt.LaMEMinputFileName]);
        else
            error(['Your LaMEM input file does not exist:' opt.LaMEMinputFileName]);
        end
    end
else
	error(['Your input directory does not exist:' opt.inputDir])
end

if ~isdir(opt.outputDir)
    mkdir(opt.outputDir);
    disp(['> Output directory does not exist. Created the following directory: ']);
    disp(['> ' opt.outputDir]);
end
    

% get orientation
if strcmp(opt.orient,'ReadFromFileName')
    if opt.Mode == '3D'
        opt.orient = getOrientation(opt.inputFileName);
    else
        opt.orient = 'HZ';
    end
end
    
if strcmp(opt.orient,'NS')
    opt.normalDir = [1 0 0];
elseif strcmp(opt.orient,'EW') 
    opt.normalDir = [0 1 0];
elseif strcmp(opt.orient,'HZ')
    opt.normalDir = [0 0 1];
else
    error('Unknown orientation')
end


if opt.useSVG
    if ~isempty(opt.pathNames)
        if ~isempty(opt.phase)
            if length(opt.pathNames)~=length(opt.phase)
                error('The lengths of opt.pathNames and opt.phase should be identical')
            end
        end
    else
        if ~isempty(opt.phase)
            warning('opt.pathNames is not specified, but opt.phase is. The user defined opt.phase will be ignored. opt.phase will be read from the SVG file.')
        end
    end
end

% % get root of the inputfilename 
% opt.inputFileNameRoot = getRootString([opt.inputDir opt.inputFileName]);
% opt.outputFileNameRoot = getRootString([opt.outputDir opt.outputFileName]);


%opt.outputFileNameRootVert = getRootString([opt.outputDir opt.outputFileNameVert]);


% %order entries according to the orientation
% if isfield(opt,'xi')
%    opt.(opt.orient).xi = opt.xi;
%    opt                 = rmfield(opt,'xi');
% end
% 
% if isfield(opt,'yi')
%    opt.(opt.orient).yi = opt.yi;
%    opt                 = rmfield(opt,'yi');
% end

% /!\/!\/!\/!\  Should be fixed  /!\/!\/!\/!\
% if isfield(opt,'zi')
%     opt.(opt.orient).zi = opt.zi;
%     opt                 = rmfield(opt,'zi');
% end

% if isfield(opt,'z')
%     opt.(opt.orient).z  = opt.z;
%     opt                 = rmfield(opt,'z');
% end



% output directory
if ~isdir(opt.outputDir)
	disp('> Your output directory does not exist yet');
    opt.outputDir = [pwd '/Output/'];
    mkdir(opt.outputDir);
    disp(['> Created output directory: ' opt.outputDir]);
end

% output file name
if ~isempty(opt.outputFileName)
    opt.outputFileName     = [opt.outputDir opt.outputFileName];
    disp(['> Output file: ' opt.outputFileName])
else
    opt.outputFileName     = [opt.outputDir 'output_interp.' opt.orient '.svg'];
    disp(['> Your output filename does not exist; The name was defined as follows:']);
    disp(['> ' opt.outputFileName])
end


if opt.writeSVG
    if ~strcmp(opt.orient,getOrientation(opt.outputFileName))
        error('Filenames of in and output files are not consistent')
    end
end

% vertical slices
if opt.Vert.write
    if ~strcmp(opt.Vert.orient,getOrientation(opt.Vert.outFileName))
        error('Filenames of in and output files are not consistent')
    end
    if ~strcmp(opt.Vert.orient,'NS') && ~strcmp(opt.Vert.orient,'EW') && ~strcmp(opt.Vert.orient,'HZ')
        error('opt.Vert.orient must be NS, EW or HZ!')
    end
    if isempty(opt.Vert.xi) || isempty(opt.Vert.yi) || isempty(opt.Vert.zi)
        error('Define opt.Vert.xi, opt.Vert.yi and opt.Vert.zi!')
    end
    if ~isempty(opt.Vert.X_to_Y) && (length(opt.Vert.X_to_Y) > 1 || opt.Vert.X_to_Y < 0)
        error('opt.Vert.X_to_Y has to be a single positive value!')
    end
    if ~isempty(opt.Vert.ref) 
        if ~(length(opt.Vert.ref) == 4)
            error('ref has to be a 4 element vector!')
        elseif ~(opt.Vert.ref(1) < opt.Vert.ref(2)) || ~(opt.Vert.ref(3) < opt.Vert.ref(4))
            error('ref should be: [x_min x_max y_min Y_max]')
        end
    end
end


if opt.writePolygons
    if opt.getPolygons == 0
        % if opt.getPolygons is active, then switch opt.getPolygons on as
        % well
        opt.getPolygons = 1;
    end
    if ~isempty(opt.PolygonsFileName)
%         opt.PolygonsFileName     = [opt.outputDir opt.PolygonsFileName];
        disp(['> LaMEM inputfile will be written to: ' opt.PolygonsFileName])
    else
        opt.PolygonsFileName     = [opt.outputDir 'LaMEMInputPolygons.bin'];
        disp(['> Your filename for the LaMEM input does not exist; write to: ' opt.PolygonsFileName])
    end
end
fprintf('\n');

end

function str = cleanString(str)

    if strcmp(str(1),'/') % absolute path

        if ~strcmp(str(end),'/')
            str = [str '/'];
        end

    else % relative path

        if ~strcmp(str(end),'/')
            str = [str '/'];
        end

        if strcmp(str(1:2),'./')
           str = str(3:end);
        end

        str = [pwd '/' str];

    end
end


function str = getRootString(str)
    Extension = str(end-6:end);
    if    ~(strcmp(Extension,'.NS.svg') | strcmp(Extension,'.EW.svg') | strcmp(Extension,'.HZ.svg'))
        error('filename must have an identifier of this type *.NS.svg or *.EW.svg or *.HZ.svg')
    end
    is = strfind(str,'/');
    ie = strfind(str,'.');
    if length(ie)<2
        error('filename must have an identifier of this type *.NS.svg or *.EW.svg or *.HZ.svg')
    end
    str = str(is(end)+1:ie(end-1)-1);
end

function orient = getOrientation(str)
    i = strfind(str,'.');
    if length(i)==1
        error(['The filename ' str ' has no orientation suffix. Please add either NS, EW, or HZ']);
    end
    orient = str(i(end-1)+1:i(end)-1);

    if ~strcmpi(orient,{'NS','EW','HZ'})
        error(['The filename ' str ' orientation suffix does not fit to database. Can be either NS, EW, or HZ']);
    end

end
