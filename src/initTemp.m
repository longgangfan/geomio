function [TypeMap, AgeMap, X2D, Y2D, X3D, Y3D, Z3D, T3D] = initTemp(opt,PathCoord, TempModel, PathLabels, TempType, nx, ny, nz, Topo, varargin)
% initTemp(opt,PathCoord, Type, PathLabels, TempType, nx, ny, nz, Topo)
% initTemp(opt,PathCoord, Type, PathLabels, TempType, nx, ny, nz, Topo, fileName)
% %% Continental crust
% TempModel{1}.TprofType   = 'linear';
% TempModel{1}.Tgradient   = [0.020]; % Tgradient  for each layer
% TempModel{1}.Depth       = [-65]*1e3; % Depth until which the given Tgradient is applied
% 
% 
% %% Ocean
% TempModel{2}.TprofType   = 'HSC'; % half space cooling model
% TempModel{2}.Age         = 80 * Myrs;
% TempModel{2}.AgeMap      = [];
% 
% PathLabels   = {'Tethys_oc','Africa_cc','Europe_cc','Adria_cc','Adria_NorthWeakZone','Mediterranean_WeakZone'};
% TempType     = [   3              1          4            4              3                     3             ]; % in drawing order from bottom to top
%
% Topo is a nx*ny matrix containing the topography information
% If Topo==[], then the Topography is deduced from the PathCoord (i.e., piecewise constant)

% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   initTemp.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

% Myrs = 3600*24*365.25*1e6;
Layers = fieldnames(PathCoord);
noLayers = length(Layers);

noPaths = length(PathLabels);

TopoMax = -660*ones(1,length(TempType))*1e3; % Initialize to bottom of the box
topLayer = zeros(1,noPaths); % Top Layer on which a path is found

X = linspace(opt.setup.x_left,opt.setup.x_left+opt.setup.W,nx)*1e3;
Y = linspace(opt.setup.y_front,opt.setup.y_front+opt.setup.L,ny)*1e3;
Z = linspace(opt.setup.z_bot,opt.setup.z_bot+opt.setup.H,nz)*1e3;
[X3D,Y3D,Z3D] = ndgrid(X , Y , Z);
X2D = X3D(:,:,1);
Y2D = Y3D(:,:,1);

T = zeros(size(X2D));
TypeMap = zeros(nx,ny);
AgeMap = zeros(nx,ny);


if isempty(Topo)
    Topo = zeros(nx,ny);
    computeTopo = true;
else
    Topo = Topo*1e3;
    computeTopo = false;
end

figure(1)
clf()
subplot(221)
hold on
axis equal
for iP = 1:noPaths
    for iL = 1:noLayers
        LayerName = Layers{iL};
        Layer = PathCoord.(LayerName);
        Paths = fieldnames(Layer);

        if not(isempty(find(LayerName=='p')))
            I = find(LayerName=='p');
            Depth = str2num(LayerName(I+1:end))*1e3;
        elseif not(isempty(find(LayerName=='m')))
            I = find(LayerName=='m');
            Depth = str2num(LayerName(I+1:end))*1e3;
            Depth = -Depth;
        else
            error('Layer name %s does not contain p or m');
        end

    
        if ~isempty(find(strcmp(Paths,PathLabels{iP}), 1))
            if Depth > TopoMax(iP)
                TopoMax(iP) = Depth;
                
                Coord = PathCoord.(Layers{iL}).(PathLabels{iP}).DrawCoord * 1e3;
                [IN,ON] = InPolygon(X2D(:),Y2D(:),Coord(1,:),Coord(2,:));
                IN = IN|ON;
                if computeTopo
                    Topo(IN) = TopoMax(iP);
                end
                TypeMap(IN) = TempType(iP);
                if strcmp(TempModel{TempType(iP)}.TprofType , 'HSC')
                    AgeMap(IN) = TempModel{TempType(iP)}.Age;
                end
            end
        end
        pcolor(X2D,Y2D,TypeMap)
        shading interp
    end
end
pcolor(X2D,Y2D,TypeMap)
shading interp



[T3D] = assignTemp(T,TempModel,TypeMap,AgeMap,Topo,nx,ny,Z);



% figure
x = (max(X(:)) + min(X(:)))/2.0;%   2000 * 1e3;
[~,Ix] = min(abs(x-X));
Iy = 1:ny;
Iz = 1:nz;
subplot(221)
plot(x*ones(size(Iy)),Y(Iy),'-r')
subplot(223)
plotTempCrossSection(1,Ix,Iy,Iz,X3D,Y3D,Z3D,T3D)
title('cross section of the y-z plane at x_{center}')


y = (max(Y(:)) + min(Y(:)))/2.0;%200 * 1e3;
[~,Iy] = min(abs(y-Y));
Ix = 1:nx;
Iz = 1:nz;
subplot(221)
plot(X(Ix),y*ones(size(Ix)),'-r')
subplot(224)
plotTempCrossSection(2,Ix,Iy,Iz,X3D,Y3D,Z3D,T3D)
title('cross section of the x-z plane at y_{center}')


% Write file
PetscVec = [nx; ny; nz; T3D(:)];
LengthFile = length(PetscVec);
PetscVec = [LengthFile; PetscVec];
if length(varargin)==1
    FileName = varargin(1);
else
    FileName = [opt.outputDir 'Temperature.bin'];
end
fprintf(' => Write Temperature file %s\n', FileName)
petscBinaryWrite(FileName,PetscVec)

end



function [T3D] = assignTemp(T,Type,TypeMap,AgeMap,Topo,nx,ny,Z)
Ztb = fliplr(Z);
% z_max = 0;
nz = length(Z);
T3D = zeros(nx,ny,nz);
Ttop = 0;
Tbot = 1300;
for ixC = 1:nx
    for iyC = 1:ny
        TypeNumber = TypeMap(ixC,iyC);
        cellType = Type{TypeNumber};
        switch cellType.TprofType
            case 'linear'
                % Assign temperatures from top to bottom
                Dtop = Topo(ixC,iyC); % Depth
                T = zeros(size(Z));
                %                     T(end) = Tbot;
                DiffZtb = abs(diff(Ztb));
                %                     T(Ztb>=Dtop) = Ttop;
                % Loop through layers of piecewise linear gradient
                cellType.noLayers = length(cellType.Tgradient);
                for iL = 1:cellType.noLayers
                    Dbot = cellType.Depth(iL);
                    I = Ztb<Dtop & Ztb>=Dbot;
                    T(I) = cellType.Tgradient(iL)*DiffZtb(I);
                    Dtop = cellType.Depth(iL);
                end
                T = cumsum(T);
                T(T>Tbot) = Tbot; % Ensures that the profile is monotonically increasing
                % The bottom of the temperature profile is linear from the
                % bottom value of the last layer to the boundary temperature
                In = find(I,1,'last');
                T(In+1:end) = linspace(T(In),Tbot,length(T)-In);
                
                T = fliplr(T); % transform back into bottom to top profile
                
            case 'HSC'
                Dtop = Topo(ixC,iyC); % Depth
                %                 Age = 40 * Myrs;
                thisZ = Z-Dtop;
                Age = AgeMap(ixC,iyC);
                Conductivity = 3;
                Rho_m = 3200;
                Tm = 1300;
                Cp = 1000;
                Kappa = Conductivity/(Rho_m*Cp);
                T = Tm*erf(abs(thisZ)./(2*sqrt(Kappa*Age)));
                
        end
        T(Z>=Topo(ixC,iyC)) = Ttop;
        T3D(ixC,iyC,:) = T;
    end
end
T3D(isnan(T3D)) = 0; % happens if the Age is 0
T3D = T3D+273;



end


function plotTempCrossSection(nDir,Ix,Iy,Iz,X3D,Y3D,Z3D,T3D)
X2D = squeeze(X3D(Ix,Iy,Iz));
Y2D = squeeze(Y3D(Ix,Iy,Iz));
Z2D = squeeze(Z3D(Ix,Iy,Iz));
T2D = squeeze(T3D(Ix,Iy,Iz));
cla
FontOpts = {'FontSize',7};
switch nDir
    case 1
        %             contourf(Y2D,Z2D,T2D-273)
        pcolor(Y2D/1e3,Z2D/1e3,T2D-273)
        shading interp
        hold on
        [~,h] = contour(Y2D/1e3,Z2D/1e3,T2D-273,0:200:1350,'Color','k');
    case 2
        %             contourf(X2D,Z2D,T2D-273)
        pcolor(X2D/1e3,Z2D/1e3,T2D-273)
        shading interp
        hold on
        [~,h] = contour(X2D/1e3,Z2D/1e3,T2D-273,0:200:1350,'Color','k');
        
        
    case 3
        %             contourf(X2D,Y2D,T2D-273)
        pcolor(X2D/1e3,Y2D/1e3,T2D-273)
        shading interp
        hold on
        [~,h] = contour(X2D/1e3,Y2D/1e3,T2D-273,0:200:1350,'Color','k');
        
    otherwise
        error('unknown nDir %.f',nDir)
end
%set(h,'ShowText','on','TextStep',get(h,'LevelStep'))
colorbar
caxis([0 1350])
end



function plotDensityCrossSection(nDir,Ix,Iy,Iz,X3D,Y3D,Z3D,T3D)

X2D = squeeze(X3D(Ix,Iy,Iz));
Y2D = squeeze(Y3D(Ix,Iy,Iz));
Z2D = squeeze(Z3D(Ix,Iy,Iz));
T2D = squeeze(T3D(Ix,Iy,Iz));

Tm = 1300+273;
Rho_m = 3300;
alpha = 3e-5;
Rho = Rho_m*(1-alpha*(T2D-Tm));

cla
switch nDir
    case 1
        %             contourf(Y2D,Z2D,T2D-273)
        pcolor(Y2D,Z2D,Rho)
        shading interp
        hold on
        [~,h] = contour(Y2D,Z2D,Rho,'Color','k');
    case 2
        %             contourf(X2D,Z2D,T2D-273)
        pcolor(X2D,Z2D,Rho)
        shading interp
        hold on
        [~,h] = contour(X2D,Z2D,Rho,'Color','k');
        
        
    case 3
        %             contourf(X2D,Y2D,T2D-273)
        pcolor(X2D,Y2D,Rho)
        shading interp
        hold on
        [~,h] = contour(X2D,Y2D,Rho,'Color','k');
        
    otherwise
        error('unknown nDir %.f',nDir)
end
set(h,'ShowText','on','TextStep',get(h,'LevelStep'))
colorbar
%     caxis([3000 3300])
end
