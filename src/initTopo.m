function [Topo, X2D, Y2D] = initTopo(opt,Volumes, PathLabels_positive, PathLabels_negative, nx, ny,varargin)
% [Topo, X2D, Y2D] = initTopo(opt,Volumes, PathLabels_positive, PathLabels_negative, nx, ny)
% [Topo, X2D, Y2D] = initTopo(opt,Volumes, PathLabels_positive, PathLabels_negative, nx, ny,filename)
%
%
% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   initTopo.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

    X = linspace(opt.setup.x_left,opt.setup.x_left+opt.setup.W,nx);
    Y = linspace(opt.setup.y_front,opt.setup.y_front+opt.setup.L,ny);
    [X2D,Y2D] = ndgrid(X , Y);
    Topo = opt.setup.z_bot*ones(nx,ny);
    nP = nx*ny;
    zbot = opt.setup.z_bot;

    d = [0,0,1]; % dir
    o = [X2D(:)';Y2D(:)';zbot*ones(1,nP)];
    
    
    %% Positive volumes
    nVol = length(PathLabels_positive);
    for iVol = 1:nVol
        Vol = Volumes.(PathLabels_positive{iVol});
        X            = Vol.CartCoord{1};
        Y            = Vol.CartCoord{2};
        Z            = Vol.CartCoord{3};
        TRI          = Vol.TRI;
        nTri = size(TRI,1);

        for iTri = 1:nTri
            %% Get coord of the corners
            XTri = X(TRI(iTri,:));
            YTri = Y(TRI(iTri,:));
            ZTri = Z(TRI(iTri,:));


            p0 = [XTri(1),YTri(1),ZTri(1)]';
            p1 = [XTri(2),YTri(2),ZTri(2)]';
            p2 = [XTri(3),YTri(3),ZTri(3)]';

            [I, ~, ~, t] = rayTriangleIntersection_vectorized (o, d, p0, p1, p2);

            z = zbot+t;
            In3 = z>Topo(I);
            I = I(In3);
            Topo(I) = z(In3);

        end
    end


    %% Negative volumes
    nVol = length(PathLabels_negative);
    for iVol = 1:nVol
        Vol = Volumes.(PathLabels_negative{iVol});
        X            = Vol.CartCoord{1};
        Y            = Vol.CartCoord{2};
        Z            = Vol.CartCoord{3};
        TRI          = Vol.TRI;
        nTri = size(TRI,1);

        for iTri = 1:nTri
            %% Get coord of the corners
            XTri = X(TRI(iTri,:));
            YTri = Y(TRI(iTri,:));
            ZTri = Z(TRI(iTri,:));


            p0 = [XTri(1),YTri(1),ZTri(1)]';
            p1 = [XTri(2),YTri(2),ZTri(2)]';
            p2 = [XTri(3),YTri(3),ZTri(3)]';

            [I, ~, ~, t] = rayTriangleIntersection_vectorized (o, d, p0, p1, p2);

            z = zbot+t;
            In3 = z<Topo(I);
            I = I(In3);
            Topo(I) = z(In3);

        end
    end
    
    % Write file
    PetscVec = [nx; ny; Topo(:)];

    LengthFile = length(PetscVec);
    PetscVec = [LengthFile; PetscVec];


    if length(varargin)==1
        FileName = varargin(1);
    else
        FileName = [opt.outputDir 'Topo.bin'];
    end
    fprintf(' => Write Topography file %s\n', FileName)
    petscBinaryWrite(FileName,PetscVec)
end

function [I, u, v, t] = rayTriangleIntersection_vectorized (o, d, p0, p1, p2)
% Ray/triangle intersection using the algorithm proposed by M??ller and Trumbore (1997).
%
% Input:
%    o : origin. 3xN matrix, where N is the number of rays
%    d : direction. 3x1 vector (same direction for all rays)
%    p0, p1, p2: vertices of the triangles. 3x1 vectors
% Output:
%    I: Index of points inside the triangle
%    u,v: barycentric coordinates.
%    t: distance from the ray origin.
% Author: 
%    Jesus Mena
%    Arthur Bauville (vectorization)

    epsilon = 0.00001;

    e1 = p1-p0;
    e2 = p2-p0;
    q  = cross(d,e2);
    a  = q*e1; % determinant of the matrix M
    f = 1/a;
    if (abs(a)<epsilon) 
        % the vector is parallel to the plane (the intersection is at infinity)
        [flag, u, v, t] = deal(0,0,0,0);
    end


    s = bsxfun(@minus,o,p0);

    u = f *   q*s;

    In = u>0.0;
    nP1 = length(find(In));
    r = cross(s(:,In),e1*ones(1,nP1));

    v = f * d*r;

    In2 = ~(v<0.0 | u(In)+v>1.0);

    t = f*e2'*r(:,In2); % verified! 

    I = find(In);
    I = I(In2);
end
