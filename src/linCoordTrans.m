function [xct,fx]=linCoordTrans(x,xc,xt)
% Transform coordinates into another linear system
%
% [xct,fx]=LinCoordTransform(x,xc,xt)
% [xct,fx]=LinCoordTransform([x1 x2],[xc1 xc2],x(:))
% 
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   linCoordTrans.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
% dxc = (xc(2)-xc(1));
% dx  = (x(2) -x(1));
% fx  = dxc /dx;
% x0c = xc(1) - (dxc/dx *x(1));
% 
% 
% xct = x0c + xt .* fx;
% % xct = x0c + (xt-x(1 )) .* fx;



dxc = abs(xc(2)-xc(1));
dx  = abs(x(2) -x(1));
fx  = dxc /dx;

xct = fx*(xt-x(1))+xc(1);

end

