function [X,Y,Z,R] = lonLatR2xyz(varargin)
%
% Converts spherical coordiantes into cartesian coordinates
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   lonLatR2xyz.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
LON = varargin{1};  % Longitude
LAT = varargin{2};  % Latitude
R   = varargin{3};  % Radius (counted from center of Earth)

if nargin>3
    topo = varargin{4};
else
    topo = zeros(size(LON(:,:,1)));
end

if nargin>4
    exaggerate = varargin{5};
else
    exaggerate = 0;
end


R(:,:,end) = R(:,:,end) + topo * exaggerate;
X          = R .* cos( deg2rad(LON) ) .* cos( deg2rad(LAT) );
Y          = R .* sin( deg2rad(LON) ) .* cos( deg2rad(LAT) );
Z          = R .* sin( deg2rad(LAT) );

end