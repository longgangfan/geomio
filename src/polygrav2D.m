function gobs = polygrav2D(x0,z0,xc,zc,rho)
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   geomIO_Options.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================%
% xc,zc corner coordinates [m] of polygon
% Vertical gravity acceleration [mGal] at query profile points x0, z0 in [m]
% RETURN: gobs [mGal]
% Implemeted after Talwani and Worzel and Landisman 1959
% Tobias Baumann, Mainz 09/2017
% =========================================================================

% constants
gamma   = 6.67*1e-11;
si2mGal = 1e5;

% is vector?
if size(x0,1)>size(x0,2)
    x0=x0';
end
if size(z0,1)>size(z0,2)
    z0=z0';
end


% polygon orientation
edge = (xc(2:end)-xc(1:end-1)) .* (zc(2:end)+zc(1:end-1));

if sum(edge) < 0 % counter-clockwise?
    xc=fliplr(xc);
    zc=fliplr(zc);
end

% alternative method to check
% rlr = sqrt((xc-max(xc)).^2 + (zc-min(zc)).^2);
% rll = sqrt((xc-min(xc)).^2 + (zc-min(zc)).^2);
% [~,n] = min(rlr);
% 
% if(rll(n-1)<rll(n+1)) % counter-clockwise ?
%      xc=fliplr(xc);
%      zc=fliplr(zc);
% end

% Vectorized implementation
X0 = repmat(x0,length(xc),1);
Z0 = repmat(z0,length(xc),1);
XC = repmat(xc,length(x0),1)';
ZC = repmat(zc,length(x0),1)';
X1 = XC-X0;
Z1 = ZC-Z0;
X2 = [X1(2:end,:); X1(1,:)];
Z2 = [Z1(2:end,:); Z1(1,:)];
r1sq = X1.^2+Z1.^2;
r2sq = X2.^2+Z2.^2;
denom=Z2-Z1;
denom(denom==0)=1e-6;
alpha = (X2-X1)./denom;
beta  = (X1.*Z2 -X2.*Z1) ./denom;
factor = beta ./ (1.0 + alpha.^2);
term1 = 0.5 .* (log(r2sq)-log(r1sq));
term2 = atan2(Z2,X2)-atan2(Z1,X1);
gobs = sum(factor.*(term1-alpha.*term2));
gobs = gobs .* 2 .* gamma .* rho .* si2mGal;

end