function writePolygons( Volumes,opt)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
% Write Volume file for LaMEM
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   writePolygons.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

fprintf([' => Write LaMEM binary input \n'])

PetscVec = [];
MaxPolyLength = 0;
MaxnoPol = 0;
noVol = length(opt.pathNames);





for iVol = 1:noVol
    
    
    
    pathLabel = opt.pathNames{iVol};
    
    disp(['write vol. ' num2str(iVol) ': '  pathLabel])
    
    cstDir    = find(opt.normalDir);
    ax        = Volumes.(pathLabel).ax;
    noPoly    = length(Volumes.(pathLabel).Polygons);
    MaxnoPol  = max(MaxnoPol, noPoly);
    
    type = Volumes.(pathLabel).type;
    phase = Volumes.(pathLabel).phase;
    
    % Coordinate at which the slice is taken (in the direction Dir)
    %         PolyStartCoord = Volumes{iSVG}.(pathLabel).Polygons{1}(1,cstDir);
    %         switch ax(cstDir)
    %             case 1
    %                 [~ , PolyStartIndex] = min(abs(setup.part_x - PolyStartCoord));
    %             case 2
    %                 [~ , PolyStartIndex] = min(abs(setup.part_y - PolyStartCoord));
    %             case 3
    %                 [~ , PolyStartIndex] = min(abs(setup.part_z - PolyStartCoord));
    %             otherwise
    %                 error('opt.dirNormal is not properly defined')
    %         end
    
    % the coordinate axes of the polygon
    Dirs = [1 2 3];
    PolyDir = Dirs(Dirs~=cstDir);

    %% this does not seem to work or be necessary    
    % correct the order of axes according to right-handed trihedron
    %[~,rhax] = sort(ax(PolyDir));
    %PolyDir = PolyDir(rhax);
    
    PolyCoord = [];
    AllPolyLength = zeros(noPoly,1);
    for iP = 1:noPoly
        PolyIndex = Volumes.(pathLabel).PolyPos';
        thisPoly = Volumes.(pathLabel).Polygons{iP};
        
        thisPolyLength = size(Volumes.(pathLabel).Polygons{iP}  ,  1);
        AllPolyLength(iP) = thisPolyLength;
        MaxPolyLength = max(MaxPolyLength,thisPolyLength);
        
        
        thisPolyCoord = zeros(2*thisPolyLength,1);
        thisPolyCoord(1:2:end-1) = thisPoly(:,PolyDir(1));
        thisPolyCoord(2:2:end)   = thisPoly(:,PolyDir(2));
        
        PolyCoord= [PolyCoord ; thisPolyCoord];
        
        
%         if opt.debug && mod(iP,10)
%             plot(thisPoly(:,PolyDir(1)),thisPoly(:,PolyDir(2)),'-o')
%             hold on
%         end
        
    end
    %         PetscVec = [PetscVec ; ax(cstDir)-1 ; opt.phase(iVol) ;  noPoly ; PolyStartIndex-1 ; AllPolyLength ; PolyCoord];
    PetscVec = [PetscVec ; (cstDir)-1 ; phase ; type ;  noPoly ; PolyIndex-1;  AllPolyLength ; PolyCoord];
%             PetscVec = [PetscVec ; (cstDir)-1 ; phase ; type ;  noPoly ; PolyIndex-1;  AllPolyLength ; PolyCoord];
end


PetscVec = [sum(noVol) ; MaxnoPol ; MaxPolyLength ; PetscVec];
LengthFile = length(PetscVec);
PetscVec = [LengthFile; PetscVec];
save('PetscVec','PetscVec')
% name of binary file
name = strsplit(opt.PolygonsFileName,'.');
if length(name) == 1
    name{2} = 'bin';
end
nameBin = [name{1} '.' num2str(opt.setup.nel_x) 'x' num2str(opt.setup.nel_y) 'x' num2str(opt.setup.nel_z) '.' num2str(opt.setup.numMark_x) 'x' num2str(opt.setup.numMark_y) 'x' num2str(opt.setup.numMark_z) '.' name{2}];
petscBinaryWrite([opt.outputDir nameBin],PetscVec)
fprintf(' > Saved binary file: %s \n', [opt.outputDir nameBin])


end

