function [ TRI ] = triangulateVolumes( Xcart, Zcart, opt )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   triangulateVolumes.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
ni = size(Xcart,1); % number of points per slice
nj = size(Xcart,2); % number of slices
nt = ni*nj;

noSlices = nj;
% 
% % X = Xcart(:);
% % Y = Ycart(:);
% % Z = Zcart(:);


% Triangulate the first and last slices(i.e. close the sides)
warning off
% dt              = delaunayTriangulation(Xcart(:,1), Zcart(:,1), [(1:ni)' , [(2:ni) 1]']);
% inside          = isInterior(dt);
% TRI_first_slice = dt(inside,:);
% 
% dt              = delaunayTriangulation(Xcart(:,end), Zcart(:,end), [(1:ni)' , [(2:ni) 1]']);
% inside          = isInterior(dt);
% TRI_last_slice  = dt(inside,:);
% TRI_last_slice  = TRI_last_slice + nt-ni;

Xtri = Xcart(:,1);
Ztri = Zcart(:,1);
TRI = delaunay(Xtri,Ztri);
% IN = inpoly([mean(Xtri(TRI),2),mean(Ztri(TRI),2)]',[Xtri,Ztri]');
IN = InPolygon(mean(Xtri(TRI),2),mean(Ztri(TRI),2),Xtri,Ztri);
TRI(~IN,:) = [];
TRI_first_slice = TRI;

Xtri = Xcart(:,end);
Ztri = Zcart(:,end);
TRI = delaunay(Xtri,Ztri);
% IN = inpoly([mean(Xtri(TRI),2),mean(Ztri(TRI),2)]',[Xtri,Ztri]');
IN = InPolygon(mean(Xtri(TRI),2),mean(Ztri(TRI),2),Xtri,Ztri);
TRI(~IN,:) = [];
TRI_last_slice = TRI + (noSlices-1)*ni;

warning on

clear Xcart Ycart Zcart

%% Get Triangulation
noTri   = 2*(ni-1)*(nj-1);
TRI = zeros(noTri,3);

s2i     = @(i,j) i'+(j-1)*ni; % function equivalent to sub2ind

% Triangulate from one slice to the other
C = 0;
for iS = 1:noSlices-1
    TRI(C + [1:ni-1],:) = [s2i(1:ni-1,iS) s2i(2:ni,iS) s2i(1:ni-1,iS+1)];
    C = C + ni-1;
    
    TRI(C + [1:ni-1],:) = [s2i(2:ni,iS) s2i(1:ni-1,iS+1) s2i(2:ni,iS+1) ];
    C = C+ni-1;
    
    
end

if opt.closedContours
    % Triangulate from the last to the first point of each slice (i.e. close contours)
    
    for iS = 1:noSlices-1
        TRI(C+1,:) = [s2i(1,iS) s2i(1,iS+1) s2i(ni,iS)];
        C = C+1;
        TRI(C+1,:) = [s2i(ni,iS) s2i(1,iS+1) s2i(ni,iS+1)];
        C = C+1;
    end


    TRI = [TRI ; TRI_first_slice ; TRI_last_slice];
end
end

