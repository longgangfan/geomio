function writeSVGVert(PathCoordVert,VolumesVert,opt)
% 
% copies the empty inputfile and saves a new svg with vertical slices
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   writeSVGVert.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
% copy svg file
%opt.Vert.outFileName = [opt.outputDir    opt.ouputFileNameRoot '_VertProj.' opt.Vert.orient '.svg'];
copyfile(opt.inputFileName,opt.Vert.outFileName);

% output 
docNode = xmlread(opt.Vert.outFileName);


DirNameCopy = opt.imgDir;
opt.imgDir  = opt.Vert.imgDir;
% correction due to different SVG height s required .... changes should be
% applied here

%% find minimum and maximum paper and real coordinates for new reference frame
% paper
xpaper_min  = [];
xpaper_max  = [];
ypaper_min  = [];
ypaper_max  = [];

layerFields = fieldnames(PathCoordVert);
numLayers   = length(layerFields);
for i = 1 : numLayers
    layer       = PathCoordVert.(layerFields{i});
    pathFields  = fieldnames(layer);
    numPaths    = length(pathFields);
    for j = 1 : numPaths
        path       = layer.(pathFields{j});
        xpaper_min = min([xpaper_min path.InputCoord(1,:)]);
        xpaper_max = max([xpaper_max path.InputCoord(1,:)]);
        ypaper_min = min([ypaper_min path.InputCoord(2,:)]);
        ypaper_max = max([ypaper_max path.InputCoord(2,:)]);
    end
end


% real

% get orientation of vertical plane
switch opt.Vert.orient
    case 'NS'
        ax = [2 3 1];
    case 'EW'
        ax = [1 3 2];
    case 'HZ'
        ax = [1 2 3];
    otherwise
        error('unknown orientation')
end

xreal_min = [];
xreal_max = [];
yreal_min = [];
yreal_max = [];
volFields = fieldnames(VolumesVert);
numVols   = length(volFields);
for i = 1 : numVols
    vol = VolumesVert.(volFields{i});
    if isstruct(vol)
        xreal_min = min([xreal_min; vol.CartCoord{ax(1)}(:)]);
        xreal_max = max([xreal_max; vol.CartCoord{ax(1)}(:)]);
        yreal_min = min([yreal_min; vol.CartCoord{ax(2)}(:)]);
        yreal_max = max([yreal_max; vol.CartCoord{ax(2)}(:)]);
    end
    
end


%% center paper coordinates
if opt.Vert.center
    for i = 1 : numLayers
        layerLabel  = layerFields{i};
        pathFields  = fieldnames(PathCoordVert.(layerLabel));
        numPaths    = length(pathFields);
        for j = 1 : numPaths
            pathLabel            = pathFields{j};
            PathCoordVert.(layerLabel).(pathLabel).InputCoord(1,:) = PathCoordVert.(layerLabel).(pathLabel).InputCoord(1,:) - xpaper_min;
            PathCoordVert.(layerLabel).(pathLabel).InputCoord(2,:) = PathCoordVert.(layerLabel).(pathLabel).InputCoord(2,:) - ypaper_min;
        end
    end
    xpaper_max = xpaper_max - xpaper_min;
    xpaper_min = 0;
    ypaper_max = ypaper_max - ypaper_min;
    ypaper_min = 0;
end



%% change proportions of x and y on paper coordinates
if opt.Vert.X_to_Y
    xr_p        = (xreal_max - xreal_min) / (xpaper_max - xpaper_min);
    yr_p        = (yreal_max - yreal_min) / (ypaper_max - ypaper_min);
    ratio       = yr_p / xr_p;
    ratio       = ratio / opt.Vert.X_to_Y;
    ypaper_min  = ypaper_min * ratio;
    ypaper_max  = ypaper_max * ratio;
    for i = 1 : numLayers
        layerLabel  = layerFields{i};
        pathFields  = fieldnames(PathCoordVert.(layerLabel));
        numPaths    = length(pathFields);
        for j = 1 : numPaths
            pathLabel            = pathFields{j};
            PathCoordVert.(layerLabel).(pathLabel).InputCoord(2,:) = PathCoordVert.(layerLabel).(pathLabel).InputCoord(2,:) * ratio;
        end
    end
end

%% simplify polygons by reducing the number of gridpoints
if opt.Vert.simplify
    theta = opt.Vert.simplify;
    for i = 1 : numLayers
        layerLabel  = layerFields{i};
        pathFields  = fieldnames(PathCoordVert.(layerLabel));
        numPaths    = length(pathFields);
        for j = 1 : numPaths
            pathLabel           = pathFields{j};
            path                = PathCoordVert.(layerLabel).(pathLabel);
            num_Coord           = size(path.InputCoord,2);
            Coords_old          = [path.InputCoord path.InputCoord(:,1)];
            Coords_new          = zeros(2,num_Coord);
            Coords_new(:,1)     = Coords_old(:,1);
            iter_new = 1;
            for k = 2 : num_Coord
                Coords          = Coords_old(:,k);
                vecTo           = Coords - Coords_new(:,iter_new);
                
                vecFrom         = Coords_old(:,k + 1) - Coords;
                dotproduct      = vecTo(1) * vecFrom(1) + vecTo(2) * vecFrom(2);
                angle           = acosd(dotproduct/(norm(vecTo) * norm(vecFrom)));
                
                if angle > theta
                    iter_new                = iter_new + 1;
                    Coords_new(:,iter_new)  = Coords;
                end                              
            end
            Coords_new          = Coords_new(:,1:iter_new);            
            
            newpath.InputCoord  = Coords_new;
            newpath.InputCoord  = [newpath.InputCoord newpath.InputCoord(:,1)];
            newpath.PathStyle   = path.PathStyle;
            newpath.Commands    = path.Commands;
            newpath.Commands.SectionLength          = [size(newpath.InputCoord,2) 0];
            PathCoordVert.(layerLabel).(pathLabel)  = newpath;
        end
    end
    
end

%% first simplify code (checks all nodes at once, faster but misses long gently curved parts)
% if opt.Vert.simplify
%     theta = opt.Vert.simplify;
%     for i = 1 : numLayers
%         layerLabel  = layerFields{i};
%         pathFields  = fieldnames(PathCoordVert.(layerLabel));
%         numPaths    = length(pathFields);
%         for j = 1 : numPaths
%             pathLabel           = pathFields{j};
%             path                = PathCoordVert.(layerLabel).(pathLabel);
%             num_Coord           = size(path.InputCoord,2);
%             Coords_old          = [path.InputCoord(:,end) path.InputCoord];
%             vectors             = [diff(Coords_old(1,:)); diff(Coords_old(2,:))];
%             vectors             = [vectors vectors(:,1)];
%             angles              = zeros(1,num_Coord);
%             for k = 1 : num_Coord
%                 dotproduct      = vectors(1,k) * vectors(1,k+1) + vectors(2,k) * vectors(2,k+1);
%                 angles(k)       = acosd(dotproduct/(norm(vectors(:,k)) * norm(vectors(:,k+1))));
%             end
%             ind                 = angles > theta;
%             ind(1)              = true;
%             newpath.InputCoord  = path.InputCoord(:,ind);
%             newpath.InputCoord  = [newpath.InputCoord newpath.InputCoord(:,1)];
%             newpath.PathStyle   = path.PathStyle;
%             newpath.Commands    = path.Commands;
%             newpath.Commands.SectionLength          = [size(newpath.InputCoord,2) 0];
%             PathCoordVert.(layerLabel).(pathLabel)  = newpath;
%             path
%             newpath
%         end
%     end
%     
% end



%% change position of reference box (this should come last)
if ~isempty(opt.Vert.ref)
    xr_p        = (xreal_max - xreal_min) / (xpaper_max - xpaper_min);
    yr_p        = (yreal_max - yreal_min) / (ypaper_max - ypaper_min);
    d_xmin      = opt.Vert.ref(1) - xreal_min;
    d_xmax      = opt.Vert.ref(2) - xreal_max;
    d_ymin      = opt.Vert.ref(3) - yreal_min;
    d_ymax      = opt.Vert.ref(4) - yreal_max;
    xpaper_min  = xpaper_min + (d_xmin / xr_p);
    xpaper_max  = xpaper_max + (d_xmax / xr_p);
    ypaper_min  = ypaper_min + (d_ymin / yr_p);
    ypaper_max  = ypaper_max + (d_ymax / yr_p);
    xreal_min   = opt.Vert.ref(1);
    xreal_max   = opt.Vert.ref(2);
    yreal_min   = opt.Vert.ref(3);
    yreal_max   = opt.Vert.ref(4);
    
end

paper = [xpaper_min xpaper_max ypaper_min ypaper_max];
real = [xreal_min xreal_max yreal_min yreal_max];

writeSVG(docNode,PathCoordVert,opt,opt.Vert.orient,VolumesVert.zi_all,opt.Vert.outFileName,1,[paper; real]);

opt.imgDir = DirNameCopy;
end

function p = perimeter(Coords)
    dx  = [diff(Coords(1,:)) Coords(1,1) - Coords(1,end)];
    dy  = [diff(Coords(2,:)) Coords(2,1) - Coords(2,end)];
    p   = sum(sqrt(dx.^2 + dy.^2));
end

function d = distance(p1,p2)
    d = sqrt((p2(1)-p1(1))^2 + (p2(2)-p1(2))^2);
end