function [Profiles] = readProfileFromSVG(opt)
%
% PathCoord = readSVG(opt)
% read paths from SVG file with multiple layers
%
% Plot paths:
% opt.debug = true;
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   readProfileFromSVG.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
% Create DOM elements
docNode = xmlread(opt.inputFileName);

% Get svg attributes
svgList     = docNode.getElementsByTagName('svg');
currentSVG  = svgList.item(0);
currentSVG.setIdAttribute('id', true) % must be defined to use .getElementById, later on
svg.height  = str2double(currentSVG.getAttribute('height'));
svg.width   = str2double(currentSVG.getAttribute('width'));

% Get the list of g nodes (layers)
layerList = docNode.getElementsByTagName('g');
noLayers = layerList.getLength;

% if isempty(opt.pathNames)
%     FillPathNames = true;
% else
%     FillPathNames = false;
% end




% Read paths
% Initialize variables
PathCoord = struct();
C = 0;
FoundProfiles = 0;
for il = 1:noLayers
    
    % Get the current g node (layer)
    currentLayer = layerList.item(il-1);
    layerLabel   = char(currentLayer.getAttribute('inkscape:label'));
    currentLayer.setIdAttribute('id', true);
    
    if strcmp(layerLabel,'Profiles')
        fprintf('Profiles layer found\n')
        FoundProfiles = 1;
        
        % Get path nodes
        pathList = currentLayer.getElementsByTagName('path');
        noPaths  = pathList.getLength;
        for ip = 1:noPaths
            currentPath = pathList.item(ip-1);
            currentPath.setIdAttribute('id', true);
            pathLabel = char(currentPath.getAttribute('label'));
            if isempty(pathLabel)
                pathLabel = char(currentPath.getAttribute('id'));
            end
            
            PathData    = char(currentPath.getAttribute('d'));
            PathStyle    = char(currentPath.getAttribute('style'));
            [InputCoord, ~, Commands]  = readPath(opt, PathData);
            
            InputCoord(:,2)      = svg.height - InputCoord(:,2);
            
            % Fill the PathCoord struct
            PathCoordProf.(layerLabel).(pathLabel).InputCoord = InputCoord;
            PathCoordProf.(layerLabel).(pathLabel).DrawCoord  = InputCoord;%DrawCoord;
            PathCoordProf.(layerLabel).(pathLabel).Commands   = Commands;
            PathCoordProf.(layerLabel).(pathLabel).PathStyle  = PathStyle;
            
            [ Profiles.(pathLabel).Lat, Profiles.(pathLabel).Lon, Profiles.(pathLabel).Depth, ~ , ~ ] = Paper2LatLonDepth( PathCoordProf, pathLabel, opt );
        end

       
    end
end
if FoundProfiles == 0
   error('SVG does not contain profiles; deactivate the option to read profiles\n'); 
end
opt.pathNames = unique(opt.pathNames);
% Check if there is a reference layer and if yes overwrites the reference
% points








end
