classdef geomIO_Options < handle %& dynamicprops
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   geomIO_Options.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
    properties
        inputDir        = './';
        outputDir       = './Output';
        
        inputFileName   = ['geomIO_IN.HZ.svg'];
        outputFileName  = [];
        
        PolygonsFileName = ['Polygons.bin'];
        LaMEMinputFileName = 'none';
        
        % Related to input file
        orient              = 'ReadFromFileName';
        
        z                  =  'ReadZFromFile';
        zi                 =  'NotSpecified';
        
        xrefPaper          = [ 0 744 ];
        yrefPaper          = [ 0 1052 ];
        
        xref               = [ 0 744 ];
        yref               = [ 0 1052 ];
        
        
        shiftPVobj        = [0 0 0];
        closedContours      = true;
        
        % Output options
        imgAdd              = true;
        interp              = true;
        debug               = false;
        getPolygons         = false;
        
        writeParaview       = false;
        writeSVG            = false;
        
        writeVertSVG        = false;
        writeVolumes         = true;
        
        % Geometry options
%         dirNormal           = [1 0 0];
        CoordinateSystem    = 'Cartesian'; % Cartesion or Spherical
        InterpType          ='PCHIP';
        transformCoord      = true; % Paper2LatLon or to whatever units CoordRef is in (in the svg file)

        % Spherical to cartesian coordinate system, using a map projection
        % note that CoordinateSystem should then be cartesian
        referenceProjection = 'NotSpecified';
        transform2Cart      = false; % Lat Lon 2 LaMEM, Cartesian / requires the Mapping Toolbox
        
        % Paths
        pathNames           = {}; % in drawing order from bottom to top
        phase               = [];
        type                = 0;

%         readPhaseFromSVG = true;
        
        DrawCoordRes        = 21; % no points per segment in DrawCoord
        
        
        % Images
        imgDir          = ['Images/']; % path from the svg file
        imgType         = 'jpg';
        imgX            = 0;
        imgY            = 0;
        
        %setup = struct('units','geo','W',0,'L',0,'originAtLLC','false','marker_x',[],'marker_y',[],'marker_z',[]);
        setup = struct('units','geo','W',0,'L',0,'H',0,'x_left',0,'y_front',0,'z_bot',0,'originAtLLC','false','marker_x',[],'marker_y',[],'marker_z',[]);
        
        Box = struct('xmin',[],'xmax',[],'ymin',[],'ymax',[]); % Coordinate of the box draw on the "Box" layer, for convenience only. No computation is done with those
        
        svg = struct('width',[],'height',[]);
        
        useSVG = true;
        
%         % Multi SVGs
%         multiSVGs= {};
%         readMultiSVGs = false;
%         inputFileNameRoot
%         outputFileNameRoot
        
        % /!\ /!\ /!\ /!\ /!\ /!\
        % Kind of dirty
        % Multi svg requires opt.(opt.orient).z etc...
        % Therefore I initialize them here
%         HZ
%         NS
%         EW

        % Vertical slices
        Vert = struct('write',false,'orient',[],'xi',[],'yi',[],'zi',[],'outFileName','outVert','imgDir','VertImg','center',false,'X_to_Y',[],'ref',[],'simplify',[]);        
        % write:        make vertical slices or not
        % orient:       orientation of slices
        % xi:           raster in x-direction
        % yi:           raster in y-direction
        % zi:           raster in z-direction
        % outFileName:  name of output file
        % imgDir:       directory for Images
        % center:       start drawing at 0,0 or not
        % X_to_Y:       scaling between x and y axis (=1: equal axis, =2: exaggerated in x-direction, =0.5 exaggerated in y-direction)
        % ref:          choose the extends of the new reference frame ([x_min x_max y_min y_max])
        % simplify:     reduces number of nodes in the output file. only takes a new node after x degrees of curvature 
        %               2 - 5 is probably a reasonable value (0.01 will still eliminate all unnecessary nodes on non curved polygons)

        % Mode: 2D or 3D, orientation checks will be jumped for 2D
        Mode = '3D';
        
        % Other stuff
        normalDir = [1 0 0];
        
        
        % LaMEM stuff
        NewLaMEM            = true;
        readLaMEM           = false;
        writePolygons       = false;
        originAtLLC         = false;
        
        svgEditor = 'Inkscape'; % 'Inkscape' or 'Illustrator'
        
        runJob = 'default';
        
        % gravity computation
        gravity = struct('drho',[],'survey_x',[],'survey_y',[],'survey_z',[],'lenUnit','km');
        
    end
    methods

    end
end