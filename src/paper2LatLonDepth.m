function [ Lat, Lon, Depth, drawDir, sliceDir ] = paper2LatLonDepth( PathCoord, pathLabel, orient, opt)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   paper2LatLonDepth.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

[  ~ , IL ] = findPathsInLayers( PathCoord, pathLabel );
    zInterp  = opt.zi(IL);

switch orient
        case 'NS'
            % Extract data
            [Lat,Depth] = extractPathsFromLayers(PathCoord,pathLabel,'DrawCoord');
            Lon         = repmat(zInterp,[size(Lat,1),1]);

            
%             plot([opt.xrefPaper(1) opt.xrefPaper(2) opt.xrefPaper(2) opt.xrefPaper(1) opt.xrefPaper(1)],...
%                  [opt.yrefPaper(1) opt.yrefPaper(1) opt.yrefPaper(2) opt.yrefPaper(2) opt.yrefPaper(1)],'o-')
% 
%             plot([opt.xrefPaper(1) opt.xrefPaper(2) opt.xrefPaper(2) opt.xrefPaper(1) opt.xrefPaper(1)],...
%                 -[opt.yrefPaper(1) opt.yrefPaper(1) opt.yrefPaper(2) opt.yrefPaper(2) opt.yrefPaper(1)]+opt.svg.height,'o-')
%             hold on
%             plot(Lat(:,1),Depth(:,1))         
   

            
            
            
            
            if opt.transformCoord
                % Scaling from paper coordinates to lat/lon/depth
                Lat     = linCoordTrans(opt.xrefPaper,opt.xref,Lat);
                Depth   = linCoordTrans(opt.yrefPaper,opt.yref,Depth);
                
                
                
                
                
                
                
                
                
                
                
                
                
            end
            
            drawDir     = [2 3];
            sliceDir    = 1;
        case 'EW'
            % Extract data
            [Lon,Depth] = extractPathsFromLayers(PathCoord,pathLabel,'DrawCoord');
            Lat         = repmat(zInterp,[size(Lon,1),1]);
            
            if opt.transformCoord
                % Scaling from Paper coordinates to lat/lon/depth
                Lon     = linCoordTrans(opt.xrefPaper,opt.xref,Lon);
                Depth   = linCoordTrans(opt.yrefPaper,opt.yref,Depth);
            end
            
            drawDir     = [1 3];
            sliceDir    = 2;
        case 'HZ'
            % Extract data
            [Lon,Lat]   = extractPathsFromLayers(PathCoord,pathLabel,'DrawCoord');
            Depth       = repmat(zInterp,[size(Lon,1),1]);
            
            if opt.transformCoord
                % Scaling from Paper coordinates to lat/lon/depth
                Lon     = linCoordTrans(opt.xrefPaper,opt.xref,Lon);
                Lat     = linCoordTrans(opt.yrefPaper,opt.yref,Lat);
            end
            
            drawDir     = [1 2];
            sliceDir    = 3;
        otherwise
            error('unknown orientation')
end
    

end

