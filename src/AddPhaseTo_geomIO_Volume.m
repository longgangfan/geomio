function Phase = AddPhaseTo_geomIO_Volume(Volumes,opt, X,Y,Z,Phase, Name, Phase_number);
%
% Phase = AddPhaseTo_geomIO_Volume(Volume, X, Y, Z, Phase, Name, Phase_number);
%
%   This adds a phase inside the Volume with the name 'Name'
%
%   Input:
%           Volume      -   Volume created with geomIO
%           opt         -   geomIO options, which should include the name
%                           of the LaMEM input file in opt.LaMEMinputFileName
%           X,Y,Z       -   3D matrixes with coordinates
%           Phase       -   3D Phase array
%           Name        -   Name of the volume
%           PhaseNumber -   Number of the Phase we will set inside the
%                           volume
%
%   Output:
%           Phase       -   Phase matrix


if  length(opt.LaMEMinputFileName)==0
    
    error('You have to indicate the LaMEM input file in opt.LaMEMinputFileName')
    
end


% Cut the volume @ the z-levels in Z
setup       =   readLaMEMinput(opt);
Polygons    =   makePolygonSlices(Volumes.(Name).CartCoord,Volumes.(Name).ax,...
    Volumes.(Name).TRI, opt.normalDir, ...
    {setup.marker_x , setup.marker_y, setup.marker_z}, opt);


z_vec   =   squeeze(Z(1,1,:));
X2d     =   squeeze(X(:,:,1));
Y2d     =   squeeze(Y(:,:,1));


for iPoly=1:length(Polygons)
    Poly    =   Polygons{iPoly};
    
    iz      =   find( abs(Poly(1,end)-z_vec)<1e-7); % index of this polygon
    
    if ~isempty(iz)
        Phase2D         =   Phase(:,:,iz);
        ind             =   find(inpolygon(X2d,Y2d,Poly(:,1),Poly(:,2)));
        Phase2D(ind)    =   Phase_number;
        Phase(:,:,iz)   =   Phase2D;
    end
    
    
    
end




