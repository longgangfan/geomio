function [InputCoord, DrawCoord, Commands, StartSubPath] = readPath(opt, varargin)
% 
% read SVG paths
% [InputCoord, DrawCoord, Commands, StartSubPath] = readPath(CoordStr);
%
% .. or transform existing input coordinates
% [InputCoord, DrawCoord, Commands, StartSubPath] = readPath(InputCoord, Commands);
%
% see http://www.w3.org/TR/SVG/paths.html#PathData for reference
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   readPath.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

if nargin == 2
    if (ischar(varargin{1}))
        CoordStr = varargin{1};
        readSVG  = true;
        createDC = true;
    else
        error('single input must be string');
    end
elseif nargin == 3
    if (ismatrix(varargin{1}) && isstruct(varargin{2}))
        InputCoord  = varargin{1};
        Commands    = varargin{2};
        readSVG  = false;
        createDC = true;
    else
        error('input must be of type array and struct');
    end
else
    error('You can either have 1 or 2 arguments');
end

if ~createDC
   error('not createDC') 
end

if readSVG

    % Read Coordinate line
    % Find comands (e.g. M, C, m, c, s, etc...)
    Commands.Index = regexp(CoordStr,['[' 'a':'d' 'f':'z' , 'A':'D' 'E':'Z' ']']);
    Commands.String = CoordStr(Commands.Index);
    Commands.Index(end+1) = length(CoordStr)+1;% the -1 is because the line end with a '"'

    % Create the InputCoord Array
    InputCoord = [];
    FirstPoint = [0;0]; % Coordinate of the first point of a path or subpath
    for iC = 1:length(Commands.String)
        Comm = Commands.String(iC);
        Str = (CoordStr(Commands.Index(iC)+1:Commands.Index(iC+1)-1));
        if strcmp(opt.svgEditor,'Illustrator')
            Str = regexprep(Str,'-',',-');
        end
        
        if ~ismember(Comm,{'H','h','V','v'})
            Coord = str2num(Str);
            Coord = reshape(Coord,2,length(Coord)/2);
        elseif strcmp(Comm,'H')
            Coord = [str2num(Str);LastCoord(2,end)];
        elseif strcmp(Comm,'h')
            Coord = [str2num(Str);0];
        elseif strcmp(Comm,'V')
            Coord = [LastCoord(1,end);str2num(Str)];
        elseif strcmp(Comm,'v')
            Coord = [0;str2num(Str)];
        end
        
        
        Commands.SectionLength(iC) = size(Coord,2);

        switch Comm
            
            case 'M' % "moveto"
                FirstPoint = Coord(:,1);
            case 'm' % "moveto"
                if iC ~= 1
                    Coord(:,1) = Coord(:,1) + InputCoord(:,end);
                end
                if Commands.SectionLength(iC)>1
                    for iPoint = 2:size(Coord,2)
                        Coord(:,iPoint) = Coord(:,iPoint) + Coord(:,iPoint-1);
                    end
                end
                FirstPoint = Coord(:,1);
                
            case {'Z' ,'z'} % "closepath"
                Commands.SectionLength(iC) = 1;
                Coord = FirstPoint;
            
            case {'L','H','V'} % "lineto"

            case {'l','h','v' } % "lineto"
                Coord(:,1) = Coord(:,1) + InputCoord(:,end);
                for iPoint = 2:size(Coord,2)
                    Coord(:,iPoint) = Coord(:,iPoint) + Coord(:,iPoint-1);
                end
   
            case 'C' % "curveto", cubic Bezier

            case 'c' % "curveto", cubic Bezier

                no_triplets = size(Coord,2)/3;
                for iT = 1:no_triplets
                    Istart = 1+(iT-1)*3;
                    if Istart == 1
                        Coord(1,1:3) = Coord(1,1:3) + InputCoord(1,end);
                        Coord(2,1:3) = Coord(2,1:3) + InputCoord(2,end);
                    else
                        Coord(1,Istart+[0:2]) = Coord(1,Istart+[0:2]) + Coord(1,Istart-1);
                        Coord(2,Istart+[0:2]) = Coord(2,Istart+[0:2]) + Coord(2,Istart-1);
                    end
                end
            
            case 'S' % "curveto", cubic Bezier
                no_triplets = size(Coord,2)/2;
                for iT = 1:no_triplets
                    Istart = 1+(iT-1)*3;
                    if Istart == 1
                        switch Commands.String(iC-1)
                            case {'C','c','S','s'}
                                FirstControlPoint = InputCoord(:,end) +  InputCoord(:,end)-InputCoord(:,end-1);
                            otherwise
                                FirstControlPoint = InputCoord(:,end);
                        end
                        Coord = [FirstControlPoint , Coord];
                    else
                        FirstControlPoint = Coord(:,end) +  Coord(:,end)-Coord(:,end-1);
                        Coord = [Coord(:,1:Istart-1) , FirstControlPoint , Coord(:,Istart:end)];
                    end
                    Commands.SectionLength(iC) = Commands.SectionLength(iC) + 1;
                    Commands.String(iC) = 'C'; % The missing control point has been added. Therefore when the new svg file is written, a C command will be written instead
                end
                
                
            case 's' % "curveto", cubic Bezier
                no_triplets = size(Coord,2)/2;
                for iT = 1:no_triplets
                    Istart = 1+(iT-1)*3;
                    if Istart == 1
                        switch Commands.String(iC-1)
                            case {'C','c','S','s'}
                                FirstControlPoint = InputCoord(:,end) +  InputCoord(:,end)-InputCoord(:,end-1);
                            otherwise
                                FirstControlPoint = InputCoord(:,end);
                        end
                        Coord = [FirstControlPoint , Coord];
                        Coord(1,2:3) = Coord(1,2:3) + InputCoord(1,end);
                        Coord(2,2:3) = Coord(2,2:3) + InputCoord(2,end);
                    else
                        FirstControlPoint = Coord(:,end) +  Coord(:,end)-Coord(:,end-1);
                        Coord = [Coord(:,1:Istart-1) , FirstControlPoint , Coord(:,Istart:end)];
                        Coord(1,Istart+[1:2]) = Coord(1,Istart+[1:2]) + Coord(1,Istart-1);
                        Coord(2,Istart+[1:2]) = Coord(2,Istart+[1:2]) + Coord(2,Istart-1);
                    end
                    Commands.SectionLength(iC) = Commands.SectionLength(iC) + 1;
                    Commands.String(iC) = 'c';
                end
           
            case 'Q' % "curveto", quadratic Bezier
                warning('"Q" command in pathdata is an untested feature')
            case 'q'
                no_duets = size(Coord,2)/2;
                for iT = 1:no_triplets
                    Istart = 1+(iT-1)*2;
                    if Istart == 1
                        Coord(1,1:2) = Coord(1,1:2) + InputCoord(1,end);
                        Coord(2,1:2) = Coord(2,1:2) + InputCoord(2,end);
                    else
                        Coord(1,Istart+[0:1]) = Coord(1,Istart+[0:1]) + Coord(1,Istart-1);
                        Coord(2,Istart+[0:1]) = Coord(2,Istart+[0:1]) + Coord(2,Istart-1);
                    end
                end
                warning('"q" command in pathdata is an untested feature')
                
            case 'T'
                no_duets = size(Coord,2)/2;
                for iT = 1:no_duets
                    Istart = 1+(iT-1)*2;
                    if Istart == 1
                        switch Commands.String(iC-1)
                            case {'Q','q','T','t'}
                                FirstControlPoint = InputCoord(:,end) +  InputCoord(:,end)-InputCoord(:,end-1);
                            otherwise
                                FirstControlPoint = InputCoord(:,end);
                        end
                        Coord = [FirstControlPoint , Coord];
                    else
                        FirstControlPoint = Coord(:,end) +  Coord(:,end)-Coord(:,end-1);
                        Coord = [Coord(:,1:Istart-1) , FirstControlPoint , Coord(:,Istart:end)];
                    end
                    Commands.SectionLength(iC) = Commands.SectionLength(iC) + 1;
                    Commands.String(iC) = 'Q'; % The missing control point has been added. Therefore when the new svg file is written, a C command will be written instead
                end
                warning('"T" command in pathdata is an untested feature')
                
            case 't'
                no_duets = size(Coord,2)/2;
                for iT = 1:no_duets
                    Istart = 1+(iT-1)*2;
                    if Istart == 1
                        switch Commands.String(iC-1)
                            case {'Q','q','T','t'}
                                FirstControlPoint = InputCoord(:,end) +  InputCoord(:,end)-InputCoord(:,end-1);
                            otherwise
                                FirstControlPoint = InputCoord(:,end);
                        end
                        Coord = [FirstControlPoint , Coord];
                        Coord(1,2) = Coord(1,2) + InputCoord(1,end);
                        Coord(2,2) = Coord(2,2) + InputCoord(2,end);
                    else
                        FirstControlPoint = Coord(:,end) +  Coord(:,end)-Coord(:,end-1);
                        Coord = [Coord(:,1:Istart-1) , FirstControlPoint , Coord(:,Istart:end)];
                        Coord(1,Istart+[1]) = Coord(1,Istart+[1]) + Coord(1,Istart-1);
                        Coord(2,Istart+[1]) = Coord(2,Istart+[1]) + Coord(2,Istart-1);
                    end
                    Commands.SectionLength(iC) = Commands.SectionLength(iC) + 1;
                    Commands.String(iC) = 'q';
                end
                warning('"t" command in pathdata is an untested feature')
                
            case 'A'
                % Unfold the list of coordinates
                ListCoord = Coord;
                ListCoord = ListCoord(:);
                
                no_blocks = length(ListCoord)/7;
                Coord = zeros(2,4*no_blocks);
                Commands.SectionLength(iC) = 4*no_blocks;
                % Coord = [rx ry ; x-axis-rotation 0 ;  large-arc-flag sweep-flag ; x y ]; % see
                % http://www.w3.org/TR/SVG/paths.html#PathDataEllipticalArcCommands
                % for detail
                for iB  = 1:no_blocks
                    IsList = 1+(iB-1)*7;
                    IsC = 1+(iB-1)*4;
                    Coord(1,IsC+0) = ListCoord(IsList+0);
                    Coord(2,IsC+0) = ListCoord(IsList+1);
                    Coord(1,IsC+1) = ListCoord(IsList+2); % aka x-axis-rotation
                    Coord(2,IsC+1) = 0;
                    Coord(1,IsC+2) = ListCoord(IsList+3); 
                    Coord(2,IsC+2) = ListCoord(IsList+4);
                    Coord(1,IsC+3) = ListCoord(IsList+5);
                    Coord(2,IsC+3) = ListCoord(IsList+6);
                end
                
                warning('"A" command in pathdata is an untested feature')
            case 'a'
                
                % Unfold the list of coordinates
                ListCoord = Coord;
                ListCoord = ListCoord(:);
                
                no_blocks = length(ListCoord)/7;
                Coord = zeros(2,4*no_blocks);
                Commands.SectionLength(iC) = 4*no_blocks;
                % Coord = [rx ry ; x-axis-rotation 0 ;  large-arc-flag sweep-flag ; x y ]; % see
                % http://www.w3.org/TR/SVG/paths.html#PathDataEllipticalArcCommands
                % for detail
                for iB  = 1:no_blocks
                    IsList = 1+(iB-1)*7;
                    IsC = 1+(iB-1)*4;
                    Coord(1,IsC+0) = ListCoord(IsList+0);
                    Coord(2,IsC+0) = ListCoord(IsList+1);
                    Coord(1,IsC+1) = ListCoord(IsList+2); % aka x-axis-rotation
                    Coord(2,IsC+1) = 0;
                    Coord(1,IsC+2) = ListCoord(IsList+3); 
                    Coord(2,IsC+2) = ListCoord(IsList+4);
                    if IsList == 1
                        Coord(1,IsC+3) = ListCoord(IsList+5)+InputCoord(1,end);
                        Coord(2,IsC+3) = ListCoord(IsList+6)+InputCoord(2,end);
                    else
                        Coord(1,IsC+3) = ListCoord(IsList+5)+Coord(1,IsC-1);
                        Coord(2,IsC+3) = ListCoord(IsList+6)+Coord(2,IsC-1);
                    end
                end

            otherwise
                error(['command ' Comm ' in the Coord string d="...", is unknown'])
        end

        InputCoord = [InputCoord , Coord ];
        LastCoord = Coord;
    end
end



if createDC
    % Create the DrawCoord array
    DrawCoord = [];
    Cend = 0;
    StartSubPath = 1;
    for iC = 1:length(Commands.String)
        Comm = Commands.String(iC);
        Cstart = Cend +1;
        Cend   = Cstart + Commands.SectionLength(iC) - 1;
        
        Coord = InputCoord(:,Cstart:Cend);
        switch Comm
            case {'M','m','L','l','H','h','V','v'}
                DrawCoord = [DrawCoord , Coord];

            case {'C','c','S','s'}
                Coord = [InputCoord(:,Cstart-1) , Coord];
                no_triplets = (size(Coord,2)-1)/3;
                for iT = 1:no_triplets
                    Istart = 2+(iT-1)*3;
                    [b] = bezier(Coord(:,Istart+[-1:2])',opt.DrawCoordRes);
                    b(1,:) = []; % Avoids doubling points at end/beginning of segments
                    DrawCoord = [DrawCoord,b'];
                end
                
            case {'Q','q','T','t'}
                Coord = [InputCoord(:,Cstart-1) , Coord];
                no_duets = (size(Coord,2)-1)/3;
                for iT = 1:no_duets
                    Istart = 2+(iT-1)*2;
                    [~,b] = bezier(Coord(:,Istart+[-1:1])',opt.DrawCoordRes);
                    b(1,:) = []; % Avoids doubling points at end/beginning of segments
                    DrawCoord = [DrawCoord,b'];
                end
            
            case {'Z','z'}
                % if the last point is different from the first one, then
                % close the contour by adding a last point with the coord a
                % the first one
                if sqrt((DrawCoord(1,end)-Coord(1))^2 + (DrawCoord(2,end)-Coord(2))^2)>1e-3
                    DrawCoord = [DrawCoord , Coord];
                end
                StartSubPath = [StartSubPath length(DrawCoord)+1];
                
                
            case {'A','a'}
                
                Coord = [InputCoord(:,Cstart-1) ; Coord];
                no_blocks = (size(Coord,2)-1)/4;
                
                for iT = 1:no_blocks
                    Is      = 2+(iT-1)*4;
                    rx      = Coord(1,Is  );
                    ry      = Coord(2,Is  );
                    phi     = Coord(1,Is+1)/180*pi;
                    fa      = Coord(1,Is+2);
                    fs      = Coord(2,Is+2);
                    x2      = Coord(1,Is+3);
                    y2      = Coord(2,Is+3);
                    x1      = Coord(1,Is-1);
                    y1      = Coord(2,Is-1);
                    
                    % Conversion from endpoint to center parameterization
                    % following: http://www.w3.org/TR/SVG/implnote.html#ArcImplementationNotes
                    
                    % Step 1
                    xp = [cos(phi) sin(phi) ; -sin(phi) cos(phi)] * [(x1-x2)/2 ; (y1-y2)/2];
                    x1p = xp(1);
                    y1p = xp(2);
                    
                    % Step 2
                    if fa==fs % equality check with some tolerance for numerical precision
                        sign = -1;
                    else
                        sign =  1;
                    end
                    cp = sign * sqrt( (rx^2*ry^2 - rx^2*y1p^2 - ry^2*x1p^2) / (rx^2*y1p^2 + ry^2*x1p^2) ) * [rx*y1p/ry ; -ry*x1p/rx];
                    cxp = cp(1);
                    cyp = cp(2);
                    
                    % Step 3
                    c = [cos(phi) -sin(phi) ; sin(phi) cos(phi)] * cp + [(x1+x2)/2 ; (y1+y2)/2];
                    cx = c(1);
                    cy = c(2);
                    
                    % Step 4 
                    u = [1;0];
                    v = [(x1p-cxp)/rx ; (y1p-cyp)/ry];
                    sign = (u(1)*v(2) - u(2)*v(1)); 
                    if abs(sign)>1e-10
                        sign = round(sign/abs(sign));
                    else 
                        sign = 1;
                    end
                    
                    theta1 = sign * acos( (u'*v) / (norm(u)*norm(v)));
                    
                    u = [(x1p-cxp)/rx ; (y1p-cyp)/ry];
                    v = [(-x1p-cxp)/rx ; (-y1p-cyp)/ry];
                    sign = (u(1)*v(2) - u(2)*v(1)); 
                    if abs(sign)>1e-10
                        sign = round(sign/abs(sign));
                    else 
                        sign = 1;
                    end
                    Delta_theta = sign * acos( (u'*v) / (norm(u)*norm(v)));
                    Delta_theta = mod(Delta_theta,2*pi);
                    
                    if fs == 0 && Delta_theta>0
                        Delta_theta = Delta_theta - 2*pi;
                    elseif fs == 1 && Delta_theta<0
                        Delta_theta = Delta_theta + 2*pi;
                    end
                    
                    theta = linspace(theta1,theta1+Delta_theta,opt.DrawCoordRes);
                    ONES = ones(1,opt.DrawCoordRes);
                    Arc = [cos(phi) -sin(phi) ; sin(phi) cos(phi)] * [rx*cos(theta) ; ry*sin(theta)] + [cx*ONES ; cy*ONES];
                    
                    DrawCoord = [DrawCoord , Arc(:,2:end)];
                    
                end
                
                
                
            otherwise
                
        end
    end
    if StartSubPath(end)~=length(DrawCoord)+1
        StartSubPath = [StartSubPath length(DrawCoord)+1];
    end
end



end % END of FUNCTION