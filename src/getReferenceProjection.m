function mstruct = getReferenceProjection(string)
% =========================================================================
% This function creates the geographic reference frame for the Alps
% and other regions
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   getReferenceProjection.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

if(strcmp(string,'Alps'))   
    mstruct = defaultm('eqdconicstd');
    mstruct.mapparallels=[43.333333 48.666666];
%     mstruct.maplonlimit = [-6 28];
%     mstruct.maplatlimit = [37 55];
    mstruct.geoid = almanac('earth','ellipsoid','km','wgs84');
    mstruct = defaultm(mstruct);
    %mstruct.origin = [42 4.5 0];
    mstruct.origin = [46 11.75 0];
    mstruct = defaultm(mstruct);
elseif (strcmp(string,'Alps_big'))
    mstruct = defaultm('eqdconicstd');
    mstruct.mapparallels=[43.333333 48.666666];
    mstruct.maplonlimit = [0 27];
    mstruct.maplatlimit = [36 56];
    
    
    mstruct.geoid = almanac('earth','ellipsoid','km','wgs84');
    mstruct = defaultm(mstruct);
    %mstruct.origin = [42 4.5 0];
%     mstruct.origin = [38.27 -1.89 0];
     mstruct.origin = [46 11.75 0];
    mstruct = defaultm(mstruct);
    
elseif (strcmp(string,'Himalaya_crosssection'))
    mstruct = defaultm('eqdconicstd');
    mstruct.mapparallels=[21 41];
%     mstruct.maplonlimit = [0 27];
%     mstruct.maplatlimit = [36 56];
    mstruct.geoid = almanac('earth','ellipsoid','km','wgs84');
    mstruct = defaultm(mstruct);
    %mstruct.origin = [42 4.5 0];
    mstruct.origin = [31 77.5 0];
    mstruct = defaultm(mstruct);
elseif (strcmp(string,'Himalaya_crosssection_GJI'))
    mstruct = defaultm('eqdconicstd');
    % refgeom=-R67/18/94/46r
    % refproj=-JD80/31/23/40/16
    mstruct.mapparallels=[23 40];
%     mstruct.maplonlimit = [0 27];
%     mstruct.maplatlimit = [36 56];
    mstruct.geoid = almanac('earth','ellipsoid','km','wgs84');
    mstruct = defaultm(mstruct);
    mstruct.origin = [31 80 0];
    mstruct = defaultm(mstruct);
elseif (strcmp(string,'Tibet3D'))
    mstruct = defaultm('eqdconicstd');
    % refgeom=-R60/10/125/45r
    % refproj=-JD90/35/20/40/24
    mstruct.mapparallels=[20 40];
%     mstruct.maplonlimit = [0 27];
%     mstruct.maplatlimit = [36 56];
    mstruct.geoid = almanac('earth','ellipsoid','km','wgs84');
    mstruct = defaultm(mstruct);
    mstruct.origin = [35 90 0];
    mstruct = defaultm(mstruct);
else
    disp('This reference key is not implemented yet');
end


end
