function [PathCoord] = interpPaths2InterLayers(PathCoord,pathnames,opt)
%
% [PathCoord] = InterpPaths2InterLayers(X,Y,PathCoord,opt)
% Plot interpolated paths 
% opt.debug = true
% opt.InterpType = 'linear'
%
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   interpPaths2InterLayers.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
% number of new slices
% Ins = ; % index of the new slices, i.e. interpolation



for k = 1:length(pathnames)
    pathLabel = pathnames{k};
    % extract this path on all layers
    [X,Y] = extractPathsFromLayers(PathCoord,pathLabel,'InputCoord');
    [ ~ , IL ] = findPathsInLayers( PathCoord, pathLabel );
    

    if max(IL) > length(opt.z)
        IL
        error('Layers of SVG file and opt.z do not fit')
    end
    
    % position of original layers in normal direction
    zSlice  = opt.z(IL);

    % position of interpolated layers in normal direction
    zInterp{k} = opt.zi(opt.zi>=min(zSlice) & opt.zi<=max(zSlice));
    
    noNewS = length(zInterp{k});
    % number of points on this path 
    noP  = size(X,1); 

    % interpolation perpendicular to the layers
    XI{k}   = zeros(noP,noNewS);
    YI{k}   = zeros(noP,noNewS);
    for iP = 1:noP
        try
        XI{k}(iP,:) = interp1( zSlice , X(iP,:) , zInterp{k},opt.InterpType);
        YI{k}(iP,:) = interp1( zSlice , Y(iP,:) , zInterp{k},opt.InterpType);        
        catch
            warning(['> !! Problem occured in ' pathLabel '; Maybe a single polygon?'])
        end
    end
    
    XIall.(pathLabel) = XI{k};
    YIall.(pathLabel) = YI{k};
end

PathCoord = insertPaths2Struct(PathCoord,XIall,YIall,pathnames,opt);


% % Draw 
% if opt.debug
%     figure('name','InterpolatedSlices')
%     for k = 1:length(pathnames)
% 
%         % number of new profiles
%         noProf            = size(XI{k},2);
% 
%         lastLayer      = sprintf('%s_p%03d',opt.orient,yInterp{k}(end));
%         pathFields     = fields(PathCoord.(lastLayer));    
%         schemaCommands = PathCoord.(lastLayer).(pathFields{end}).Commands;
% 
%         % this has to be changed later on
%         PathStyle      = PathCoord.(lastLayer).(pathFields{end}).PathStyle;
% 
%         for il = 1:noProf
% 
%             % Create path strings
%             [ PathData ] = writePathData( schemaCommands, [XI{k}(:,il),YI{k}(:,il)]);
% 
%             % Read path and extract coordinates
%             [InputCoord, DrawCoord, Commands] = readPath(opt,PathData);
% 
%             hold on
%             drawPath( PathStyle, InputCoord, DrawCoord, Commands,  'debug' );
%         end
%     end
% end


fprintf('\n');

end
