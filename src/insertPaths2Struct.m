function [PathCoordNew] = insertPaths2Struct(PathCoord,Xall,Yall,pathnames,opt)
%
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   writePathData.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

% Initiate the structure
noLayers    = length(opt.zi);
for il = 1:noLayers
    if (opt.zi(il))<0       
        layerField = ['m' strrep(num2str(abs(opt.zi(il))),'.','p')];
    else
        layerField = ['p' strrep(num2str(abs(opt.zi(il))),'.','p')];
    end

PathCoordNew.(layerField) = struct();
end



noPaths = length(pathnames);

for k = 1:noPaths
    pathLabel = pathnames{k};
    [ LayersCell , IL ] = findPathsInLayers( PathCoord, pathnames{k} );
    ySlice  = opt.z(IL);
    yInterp = opt.zi(opt.zi>=min(ySlice) & opt.zi<=max(ySlice));
    noLayers    = length(yInterp);
    for il = 1:noLayers
        if (yInterp(il))<0-1e-8
            layerField = ['m' strrep(num2str(abs(yInterp(il))),'.','p')];
        else
            layerField = ['p' strrep(num2str(abs(yInterp(il))),'.','p')];
        end

        % get commands
        Commands = PathCoord.(LayersCell{1}).(pathLabel).Commands;
        PathStyle = PathCoord.(LayersCell{1}).(pathLabel).PathStyle;
        Phase = PathCoord.(LayersCell{1}).(pathLabel).Phase;
        
        X = Xall.(pathLabel);
        Y = Yall.(pathLabel);
        
        [PathCoordNew.(layerField).(pathLabel).InputCoord,...
            PathCoordNew.(layerField).(pathLabel).DrawCoord,...
            PathCoordNew.(layerField).(pathLabel).Commands, PathCoordNew.(layerField).(pathLabel).StartSubPath] = readPath(opt, [X(:,il) Y(:,il)]', Commands);
        
        PathCoordNew.(layerField).(pathLabel).PathStyle = PathStyle;
        PathCoordNew.(layerField).(pathLabel).Phase = Phase;
    end
end

end
