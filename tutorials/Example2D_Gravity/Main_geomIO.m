% =========================================================================
% Example code to compute a 2D gravity anomaly of several polygonial shapes
% that are stored on a single layer in the PathCoord structure
% using geomIO
%
%
% T.S. Baumann, Mainz, 10/2018
% =========================================================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   Main_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================


% clean up
clear 
close all


% settings
opt                 = geomIO_Options();
opt.inputDir        = ['./Input'];
opt.outputDir       = ['./Output'];

opt.inputFileName   = ['2Dshapes.svg'];

% run geomIO
PathCoord           = run_geomIO(opt,'2D');

x0    = -400:400; % Profile m
z0    = zeros(size(x0));


% selection of shapes and density asignment
paths = {
    'shapeA',  200
    'shapeB',  300
    'shapeC',  300
    'shapeD', -100
     };
opt.pathNames    = {paths{:,1}}; 
opt.gravity.drho = [paths{:,2}]; 
 

% Show geometries and gravity signal
plotgravSVG(PathCoord,x0,z0,[],[], opt);
