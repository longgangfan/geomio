% ==========================================
% How to assign phases to a rabbit in 2D?
% ==========================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   Main_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
clc, clear, close all

%% Initialize geomIO and extract coordinates
opt                 = geomIO_Options(); 
opt.inputFileName   = ['./Input/LapinouAndFriends.svg'];
%opt.inputFileName   = ['./Input/Ellipse.svg'];
[PathCoord]  = readSVG(opt); 


%% Define a set of markers
xmin = 0; xmax = opt.svg.width; 
ymin = 0; ymax = opt.svg.height;
[Xp,Yp]      = ndgrid(xmin:15:xmax,ymin:15:ymax);
Phase        = zeros(size(Xp));

%% Assign phase to markers
tic
Phase = assignPhase2Markers(PathCoord, opt, Xp, Yp, Phase);
toc
%% Plot your initial drawing
figure(1)
plotSVG(PathCoord,opt)

%% Plot the markers + the contours of your drawing
figure(2)
% Retrieve the colormap of your initial drawing
[CMAP, PhaseMax] = getColormapFromPathCoord(PathCoord, opt); 
colormap(CMAP)
hold on
caxis([-.5 PhaseMax+.5])
colorbar('YTick',0:PhaseMax)


scatter(Xp(:),Yp(:),20,Phase(:),'filled');
plotSVG(PathCoord,opt,0)

axis equal



