% ==========================================
% Basic 3D example combined with a LaMEM particle-based model
% ==========================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   Main_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

clc, clear, close all


% Tell the code where the LaMEM matlab routines are
addpath(genpath('../../src'))
addpath(genpath('~/WORK/LaMEM/LaMEM/matlab/'))


LaMEM_Parallel_output   =    0;

RandomNoise             =   logical(0); % add random noise to particles?

LaMEM_input_file        =   'Input/RabbitAndCat.dat';

%% Compute 3D grid, depending on whether we are on 1 or >1 processors
if ~LaMEM_Parallel_output
    % In the other case, we create a setup for 1 processor and defined the
    % parameters here.
    % Important: the resolution you use here should be identical to what
    % is specified in then *.dat file!
    disp(['Creating setup for 1 processor using LaMEM file: ', LaMEM_input_file])
    
    % Read info from LaMEM input file & create 3D grid
    [npart_x,npart_y,npart_z,Grid,X,Y,Z,W,L,H] =   LaMEM_ParseInputFile(LaMEM_input_file);
    
    nump_x              =   Grid.nel_x*npart_x;
    nump_y              =   Grid.nel_y*npart_y;
    nump_z              =   Grid.nel_z*npart_z;
    Parallel_partition  =   [];   % since we run this on one code
    
else
    % We perform a paralel simulation; or this a 'ProcessorPartitioning'
    % file shoule be created first by running LaMEM on the desired # of
    % processors as:
    %   mpiexec -n 2 ../../bin/opt/LaMEM -ParamFile Subduction2D_FreeSlip_MATLABParticles_Linear_DirectSolver.dat -mode save_grid
    disp(['Creating setup in parallel using LaMEM file: ', LaMEM_input_file])
    
    % Define parallel partition file
    Parallel_partition                          =   'ProcessorPartitioning_4cpu_4.1.1.bin'
    
    % Load grid from parallel partitioning file
    [npart_x,npart_y,npart_z]  =   LaMEM_ParseInputFile(LaMEM_input_file);
    [X,Y,Z,xcoor,ycoor,zcoor,Xpart,Ypart,Zpart] =   FDSTAGMeshGeneratorMatlab(npart_x,npart_y,npart_z,Parallel_partition,RandomNoise);
    
    % Update other variables
    nump_x  = size(X,2);
    nump_y  = size(X,1);
    nump_z  = size(X,3);
    
    % Domain parameters
    W       =   xcoor(end)-xcoor(1);    % x-dir
    L       =   ycoor(end)-ycoor(1);    % y-dir
    H       =   zcoor(end)-zcoor(1);    % z-dir
end
%==========================================================================


%==========================================================================
% READ IN THE geomIO DATA
%==========================================================================
opt                     = geomIO_Options();
opt.DrawCoordRes        = 10;
opt.inputFileName       = ['Input/RabbitAndCat.HZ.svg'];
opt.writeParaview       = true;
opt.readLaMEM           = true;
opt.LaMEMinputFileName  = LaMEM_input_file;
opt.writePolygons       = true;
[~, Volumes]            = run_geomIO(opt);


%==========================================================================
% CREATE MODEL INPUT FROM geomIO VOLUMES
%==========================================================================
Phase 	=   ones(size(X));                 % initialize to have mantle phase
Temp 	=   0*ones(size(Phase));   

% use the geomIO volume to set a phase inside
Phase   =   AddPhaseTo_geomIO_Volume(Volumes,opt, X,Y,Z,Phase, 'Box',           1); 
Phase   =   AddPhaseTo_geomIO_Volume(Volumes,opt, X,Y,Z,Phase, 'CatAndRabbit',  2); 




%==========================================================================
% PREPARE DATA FOR VISUALIZATION/OUTPUT (no need to change this)
%==========================================================================
% Prepare data for visualization/output
A       = struct('W',[],'L',[],'H',[],'nump_x',[],'nump_y',[],'nump_z',[],'Phase',[],'Temp',[],'x',[],'y',[],'z',[],'npart_x',[],'npart_y',[],'npart_z',[]);

Phase    = permute(Phase,[2 1 3]);
Temp     = permute(Temp, [2 1 3]);

% Linear vectors containing coords
x        =  X(1,:,1);
y        =  Y(:,1,1);
z        =  Z(1,1,:);
X        =  permute(X,[2 1 3]);
Y        =  permute(Y,[2 1 3]);
Z        =  permute(Z,[2 1 3]);

A.W      =  W;
A.L      =  L;
A.H      =  H;
A.nump_x =  nump_x;
A.nump_y =  nump_y;
A.nump_z =  nump_z;
A.Phase  =  Phase;
A.Temp   =  Temp;
A.x      =  x(:);
A.y      =  y(:);
A.z      =  z(:);
A.Xpart  =  X;
A.Ypart  =  Y;
A.Zpart  =  Z;
A.npart_x=  npart_x;
A.npart_y=  npart_y;
A.npart_z=  npart_z;

% PARAVIEW VISUALIZATION
FDSTAGWriteMatlab2VTK(A,'BINARY'); % default option

% SAVE PARALLEL DATA (parallel)
FDSTAGSaveMarkersParallelMatlab(A,Parallel_partition);









