% =========================================================================
% Example code to compute a 3D Bouguer anomaly using geomIO
% This is a benchmark with a buried sphere at 5km depth and a radius of 5km
% 
% T.S. Baumann, Mainz, 02/2018
% =========================================================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   Main_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
clc, clear, close all


% geomIO settings
opt                 = geomIO_Options();
opt.inputDir        = ['./Input'];
opt.outputDir       = ['./Output'];
opt.inputFileName   = ['Sphere.HZ.svg'];
opt.LaMEMinputFileName ='gravity.dat';
opt.readLaMEM       = true;
opt.writeParaview   = true;
opt.interp = true;
opt.zi = (-10.9:0.1:-1.1);
opt.getPolygons= true;

% Create survey grid
xs = linspace(-10,10,100);
ys = linspace(-10,10,100);
[opt.gravity.survey_x, opt.gravity.survey_y] = meshgrid(xs,ys);
opt.gravity.survey_z                         = zeros(size(opt.gravity.survey_y));
opt.gravity.lenUnit='km';

% Density assignment
paths = {
    'sphere',300,1
     };
opt.pathNames    = {paths{:,1}}; 
opt.gravity.drho = [paths{:,2}]; 
opt.phase        = [paths{:,3}];  

% Run geomIO
[PathCoord,Volumes,opt] = run_geomIO(opt,'default');  

% Show SVG
plotSVG(PathCoord,opt)

% Compute gravity
dgz_P=polygrav3D(PathCoord,opt,'PathCoord');
dgz_V=polygrav3D(Volumes,opt,'Volumes');

% Compute analytical signal
R = 5e3;
b = abs(opt.gravity.survey_z-6e3);
r = sqrt((opt.gravity.survey_x*1e3).^2+(opt.gravity.survey_y*1e3).^2);
dgz_ana = gravSphere(R,opt.gravity.drho(1),r,b);



% Plot
figure('name','Gravity signal (dgz)')
hold on
plot(diag(r),diag(dgz_P{end}))
plot(diag(r),diag(dgz_V{end}))
plot(diag(r),diag(dgz_ana))
xlabel('Distance in meters')
ylabel('dgz in mGal')
legend({'Signal computed from PathCoord structure' 'Signal computed from Volume objects' 'Analytical solution'})

% The accuracy increases when opt.zi is increased, or, in case of volumes,
% the marker resolution in the inputfile is increased







