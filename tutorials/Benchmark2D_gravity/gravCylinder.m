function gobs = gravCylinder(x0,z0,xc,zc,rho,R)
%
% xc,zc centre of mass of the cylinder [m]
% Vertical gravity acceleration [mGal] at query point x0, z0 [m]
% RETURN: gobs [mGal]
% Tobias Baumann, Mainz 09/2017
% ========================================================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   gravCylinder.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

% constants
gamma   = 6.67*1e-11;
si2mGal = 1e5;

% gravity acceleration in mGal
dz = z0-zc;
dx = x0-xc;
gobs = abs(dz) ./ (abs(dx).^2 + abs(dz).^2);
gobs = 2 .* pi .* gamma .* R.^2 .* rho .* gobs .* si2mGal;
end