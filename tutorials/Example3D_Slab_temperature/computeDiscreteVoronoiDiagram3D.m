function [VOR] = computeDiscreteVoronoiDiagram3D(X,Y, Z,xP,yP, zP, maxIt, UpDir)

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   gravCylinder.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

if maxIt == 0
    maxIt = realmax;
end

VOR = zeros(size(X)); % closest point for all cells
tic
xmax = max(X(:));
xmin = min(X(:));
ymax = max(Y(:));
ymin = min(Y(:));
zmax = max(Z(:));
zmin = min(Z(:));

nx = size(X,1);
ny = size(X,2);
nz = size(X,3);

dx = (xmax-xmin)/(nx-1);
dy = (ymax-ymin)/(ny-1);
dz = (zmax-zmin)/(nz-1);

if ( xmin>min(xP(:)) || xmax<max(xP(:)) ||...
     ymin>min(yP(:)) || ymax<max(yP(:)) || ...
     zmin>min(zP(:)) || zmax<max(zP(:)) )
    
    warning('Some points are outside the grid');

end

%% Set of Points
nP = length(xP(:));


% load('Data')
%% Misc
% CMAP = [1 1 1 ; prism(nP)];
CMAP = [1 1 1 ; rand(nP,3)];


%% List of cells claimed
Lc = cell(1,nP);
sizeLc = nx*2+ny*2+nz*2;
for iP = 1:nP
    Lc{iP} = zeros(1,sizeLc);
end
LcCounter = zeros(1,nP);

%% List of neighbour cells
Lb = cell(1,nP);
sizeLb = sizeLc*4;
for iP = 1:nP
    Lb{iP} = zeros(1,sizeLb);
end
LbCounter = zeros(1,nP);

%% Every point claims the cell it is in
IX = floor((xP(:)-xmin)/dx)+1;
IY = floor((yP(:)-ymin)/dy)+1;
IZ = floor((zP(:)-zmin)/dz)+1;

%Lc = cell(1,nP);



for iP=1:nP
    IC = IX(iP) + (IY(iP)-1)*nx + (IZ(iP)-1)*nx*ny;
    Lc{iP}(1) = IC;
    LcCounter(iP) = 1;
    VOR(IC) = iP;
end


% figure
% hold on
% Color = rand(nP,3);
% for iP = 1:nP
%     I = VOR==iP;
%     plot3(X(I), Y(I), Z(I), 's', 'MarkerFaceColor', Color(iP,:), 'MarkerEdgeColor', Color(iP,:))
% end






hold on
%% Create the list of neighbours
IxN = [-1 1  0 0  0 0];
IyN = [ 0 0 -1 1  0 0];
IzN = [ 0 0  0 0 -1 1];
NeighList = zeros(1,6);
it = 0;
while sum(LcCounter)>0 && it<maxIt
    it = it+1;
    for iP = 1:nP
        
        
        LbCounter(iP) = 0;
        for iC = 1:LcCounter(iP)
            I = Lc{iP}(iC) ;
            ix = mod(I,nx);
            ix(ix==0) = nx;
            iy = (I-ix)/nx+1;
            iy = mod(iy,ny);
            iy(iy==0) = ny;
            iz = (I-ix-(iy-1)*nx)/(nx*ny) + 1; 

            NeighList = (ix+IxN + (iy-1+IyN)*nx + (iz-1+IzN)*nx*ny);
            NeighList(ix+IxN<=0) = 0;
            NeighList(ix+IxN>nx) = 0;
            NeighList(iy+IyN<=0) = 0;
            NeighList(iy+IyN>ny) = 0;
            NeighList(iz+IzN<=0) = 0;
            NeighList(iz+IzN>nz) = 0;
            Lb{iP}(LbCounter(iP)+[1:6]) = NeighList;
           
            LbCounter(iP) = LbCounter(iP)+6;
        end
       % Lb{iP}(1:LbCounter(iP)-1);
        uniqueLb = unique(Lb{iP}(1:LbCounter(iP)));
        uniqueLb(uniqueLb == 0) = [];
        lengthUnique = length(uniqueLb);
        Lb{iP}(1:lengthUnique) = uniqueLb;
        LbCounter(iP) = lengthUnique;     
        

        
        ClaimedCells = Lb{iP}(1:LbCounter(iP));

        NonClaimedCells = ClaimedCells(VOR(ClaimedCells)==0);
        AlreadyClaimedCells = ClaimedCells(VOR(ClaimedCells)~=0);
        
        CellsToTransferToThisPoint = [];
        if ~isempty(AlreadyClaimedCells)
            
            %% getting distance from the already claimed cells to their current owner
            % and from those cells to this Point
            AlreadyClaimedCellsBelongTo = VOR(AlreadyClaimedCells);
            % AlreadyClaimedCells
            dZFromAlreadyClaimingPoint = Z(AlreadyClaimedCells) - zP(AlreadyClaimedCellsBelongTo);
            dYFromAlreadyClaimingPoint = Y(AlreadyClaimedCells) - yP(AlreadyClaimedCellsBelongTo);
            dXFromAlreadyClaimingPoint = X(AlreadyClaimedCells) - xP(AlreadyClaimedCellsBelongTo);
            
            dFromAlreadyClaimingPoint = dXFromAlreadyClaimingPoint.^2 + dYFromAlreadyClaimingPoint.^2 + dZFromAlreadyClaimingPoint.^2;
            dZFromThisPoint = Z(AlreadyClaimedCells) - zP(iP);
            dYFromThisPoint = Y(AlreadyClaimedCells) - yP(iP);
            dXFromThisPoint = X(AlreadyClaimedCells) - xP(iP);
            dFromThisPoint = dXFromThisPoint.^2 + dYFromThisPoint.^2 + dZFromThisPoint.^2;
            
            %% Distance check
            CellsToTransferToThisPoint = AlreadyClaimedCells(dFromThisPoint<dFromAlreadyClaimingPoint);
            
            
        end
        CheckList = [NonClaimedCells CellsToTransferToThisPoint];
%         if (it<2) %% Check if it is above or below in the first few iterations
%             
%             I = iP ;
%             ix = mod(I,size(xP,1));
%             ix(ix==0) = size(xP,1);
%             iy = (I-ix)/size(xP,1)+1;
%             
%             ix(ix==size(xP,1)) = ix-1;
%             iy(iy==size(xP,2)) = iy-1;
% 
%             
%             U = [xP(ix+1,iy  )-xP(ix,iy) ; yP(ix+1,iy  )-yP(ix,iy) ; zP(ix+1,iy  )-zP(ix,iy)];
%             V = [xP(ix  ,iy+1)-xP(ix,iy) ; yP(ix  ,iy+1)-yP(ix,iy) ; zP(ix  ,iy+1)-zP(ix,iy)];
%             UxV = cross(U,V);
%             
%             W = [X(CheckList)-xP(iP);Y(CheckList)-yP(iP);Z(CheckList)-zP(iP)];
%             
%             UxV_dot_W = UxV(1)*W(1,:) + UxV(2)*W(2,:) + UxV(3)*W(3,:);
%             CheckList(UpDir*UxV_dot_W<0) = [];
%         end
        
        %% Attributing cells to this point
        LcCounter(iP) = length(CheckList);
        Lc{iP}(1:LcCounter(iP)) = CheckList;
        VOR(Lc{iP}(1:LcCounter(iP))) = iP;
    end
    

    


% cla
% for iP = 1:iP
%     I = VOR==iP;
%     plot3(X(I), Y(I), Z(I), 's', 'MarkerFaceColor', Color(iP,:), 'MarkerEdgeColor', Color(iP,:))
% end
% drawnow
end
    toc
