function [Dist, VOR, NanList] = getDistanceFromSlab(X,Y,Z,thisVolume, maxDist, UpDir)
% [Dist, VOR] = getDistanceFunction(X,Y,Z,thisVolume, maxDist, UpDir)
% where Dist is a 3D matrix containing the distance between points of the grid
% (X,Y,Z) and the shape defined by thisVolume. Dist can be used to compute the
% temperature below a slab.
% thisVolume is e.g. Volumes.myVolume
% maxDist is the maximum distance of points computed. It is used to accelerate
% the computation of the Voronoi diagram by computing only part of it. 
% It Should be large enough so % that the temperature function (e.g. Half-space cooling model)
% reaches the mantle temperature. In doubt specify MaxDist = realmax;
% UpDir = -1 or 1 defines the direction of points inside or outside the
% slab. Generally -1 should work. If results are opposite though, use 1
% instead.
% VOR is the approximate Voronoi diagram computed with the function computeDiscreteVoronoiDiagram3D 

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   gravCylinder.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   geomIO_Options.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
V = thisVolume;
xP = V.CartCoord{V.ax(1)};
yP = V.CartCoord{V.ax(2)};
zP = V.CartCoord{V.ax(3)};


dx = X(2,1,1) - X(1,1,1);
dy = Y(1,2,1) - Y(1,1,1);

maxIt = maxDist/(sqrt(dx^2+dy^2));
tic
[VOR] = computeDiscreteVoronoiDiagram3D(X,Y,Z,xP,yP,zP, maxIt);
TOC = toc;
fprintf('Compute the discrete Voronoi Diagram: %.2f s\n', TOC);

% Only for test: should be considered solved
% VOR(end) = 1;
I = VOR==0;
VOR(I) = 1;
Dist = (sqrt((X-xP(VOR)).^2 + (Y-yP(VOR)).^2 + (Z-zP(VOR)).^2));
%Dist(I) = nan;
I0 = I;

%% Check if it's below or above the slab
x = xP;
y = yP;
z = zP;
xP = xP(:)';
yP = yP(:)';
zP = zP(:)';

I = ~I;
ux = [diff(x,1,1) ; x(end,:)-x(end-1,:)];
uy = [diff(y,1,1) ; y(end,:)-y(end-1,:)];
uz = [diff(z,1,1) ; z(end,:)-z(end-1,:)];
U =  [ ux(VOR(I))'; uy(VOR(I))'; uz(VOR(I))'];
vx = [diff(x,1,2) x(:,end)-x(:,end-1)];
vy = [diff(y,1,2) y(:,end)-y(:,end-1)];
vz = [diff(z,1,2) z(:,end)-z(:,end-1)];
V =  [ vx(VOR(I))'; vy(VOR(I))'; vz(VOR(I))'];

W = [X(I)'-xP(VOR(I)) ; Y(I)'-yP(VOR(I)) ; Z(I)'-zP(VOR(I))]; % Mesh point to grid point

UxV = cross(U,V,1); 
%UxW = cross(U,W,1);  

UxV_dot_W = UxV(1,:).*W(1,:) + UxV(2,:).*W(2,:) + UxV(3,:).*W(3,:);

I2 = find(I);
I2(UpDir*UxV_dot_W<0) = [];

%Dist(I2) = nan;

NanList = false(size(Dist));
NanList(I0) = true;
NanList(I2) = true;

%Dist(NanList) = nan;


