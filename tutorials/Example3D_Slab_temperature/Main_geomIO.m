% ==========================================
% Basic geodynamic example
% Drawn from top
% With linear and half-space cooling temperature profiles
% ==========================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   Amin_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
clc, clear, close all
computeVoronoi = 1 % 1 to (re)compute the Voronoi diagram

%% Read geometry for the PAC slab
opt = geomIO_Options();
opt.zi = [0:-1:-9 -10:-20:-560]; % Controls the resolution of the Volumes
opt.closedContours = false;
opt.inputFileName = ['Input/PAC_slab.HZ.svg'];
[PathCoord, Volumes] = run_geomIO(opt);




%% Read geometry for the PHS slab
opt = geomIO_Options();
opt.zi = [0:-.5:-9 -10:-5:-140];
opt.inputFileName = ['Input/PHS_slab.HZ.svg'];
[~, Volumes_PHS] = run_geomIO(opt);

%% Add Volumes_PHS to Volumes
VolumeLabels = fieldnames(Volumes_PHS);
noVol = length(VolumeLabels);
for iVol = 1:noVol
    Volumes.(VolumeLabels{iVol}) = Volumes_PHS.(VolumeLabels{iVol});
end


%% Define the 3D temperature grid
V = Volumes.PAC_slab;
xP = V.CartCoord{V.ax(1)};
yP = V.CartCoord{V.ax(2)};
zP = V.CartCoord{V.ax(3)};
nx = 32; % coarse resolution
ny = 32; 
nz = 32;
xmin = opt.Box.xmin; xmax = opt.Box.xmax;
ymin = opt.Box.ymin; ymax = opt.Box.ymax;
zmin = min(zP(:));   zmax = max(zP(:));

% Add some padding to the grid
xmax = xmax+(xmax-xmin)*.1;
ymax = ymax+(ymax-ymin)*.1;
zmin = zmin-(zmax-zmin)*.1;
zmax = 0.0;

[X,Y,Z] = ndgrid(linspace(xmin,xmax,nx), linspace(ymin,ymax,ny), linspace(zmin,zmax,nz));


%% get Distance from slabs for all the points of the grid
if (computeVoronoi)
    maxDist = 0; %realmax;
    VolumeLabels = fieldnames(Volumes);
    noVol = length(VolumeLabels);
    for iVol = 1:noVol
        % Choose UpDir: empirical, depends on the order of drawn points
        if iVol == 1
            UpDir = -1;
        else
            UpDir = 1;
        end
        thisVolume = Volumes.(VolumeLabels{iVol});
        [Distance_All.(VolumeLabels{iVol}), VOR, NanList.(VolumeLabels{iVol})] = getDistanceFromSlab(X,Y,Z,thisVolume, maxDist, UpDir);
    end
    save('Output/Distance_All','Distance_All', 'NanList');
else
    load('Output/Distance_All','Distance_All', 'NanList');
end





%% Create a composite Distance matrix by taking the minimum of every Distance_All matrices
VolumeLabels = fieldnames(Volumes);
noVol = length(VolumeLabels);

Dist = Distance_All.(VolumeLabels{1});
IndexVol = ones(size(Dist)); % Stores the index of the Volumes to which a given grid point is closest
for iVol = 2:noVol
    [Dist] = min(Dist,   Distance_All.(VolumeLabels{iVol}) ,'omitnan');
    IndexVol(Distance_All.(VolumeLabels{iVol}) == Dist) = iVol;
end

for iVol = 1:noVol
    thisNanList = (NanList.(VolumeLabels{iVol}));
    SelectDist = Dist(IndexVol==iVol);
    SelectDist(thisNanList(IndexVol==iVol)) = nan;
    Dist(IndexVol==iVol) = SelectDist;
end

% All IndexVol >= 2 belong to the same slab, so, give the same Index
IndexVol(IndexVol>=2) = 2;

% Attributes Index 3 to places where there are NaNs
IndexVol(isnan(Dist)) = 3;

% Dist for grid points not included in any slab (i.e. crust and mantle above the slab) is the depth
Dist(isnan(Dist)) = abs(Z(isnan(Dist)));
Dist = Dist*1e3; % Convert from km to m



%% Define temperature profiles
Myrs = 3600*24*365.25*1e6;
% PAC slab
TempModel{1}.TprofType   = 'HSC'; % half space cooling model
TempModel{1}.Age         = 150 * Myrs;
TempModel{1}.AgeMap      = [];

% PHS slab
TempModel{2}.TprofType   = 'HSC'; % half space cooling model
TempModel{2}.Age         = 20 * Myrs;
TempModel{2}.AgeMap      = [];

% Amurian and NAM plates
TempModel{3}.TprofType   = 'HSC'; % half space cooling model
TempModel{3}.Age         = 50 * Myrs; % This value is not geologically informed
TempModel{3}.AgeMap      = [];


%% Assign Temperature to grid according to Distance and temperature profiles using a half-space cooling model
TypeMap = ones(nx,ny);
AgeMap = ones(nx,ny) * TempModel{1}.Age;
TopoMap = 0*ones(nx,ny);

Dtop = 0.0;%TopoMap(ixC,iyC); % Depth
Age = TempModel{1}.Age;
Conductivity = 3;
Rho_m = 3200;
Tm = 1300;
Cp = 1000;
Kappa = Conductivity/(Rho_m*Cp);

T3D = zeros(size(X));
for iTemp = 1:3
    Age = TempModel{iTemp}.Age;
    T3D(IndexVol==iTemp) = Tm*erf(abs(Dist(IndexVol==iTemp))./(2*sqrt(Kappa*Age)));
end
T3D = T3D+273;



%% Plot volumes
figure(1)
plotVolumes(Volumes)


%% Plot temperature
figure(2)
Tisotherms = [1250,850,450,50]
isotermColors = {'r','y','g','b'}
for i = 1:4
p = patch(isosurface(X,Y,Z,T3D,Tisotherms(i)+273));
p.FaceColor = isotermColors{i};
p.FaceAlpha = .5;
p.EdgeColor = 'none';
end
camlight head
lighting phong
view(-30,10)
legend( sprintf('T = %.0f %s',Tisotherms(1),char(176)),...
        sprintf('T = %.0f %s',Tisotherms(2),char(176)),...
        sprintf('T = %.0f %s',Tisotherms(3),char(176)),...
        sprintf('T = %.0f %s',Tisotherms(4),char(176)))

