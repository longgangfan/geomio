![](doc/geomIO_logo.png)

### What is this repository for? ###

**geomIO** is a MATLAB-based toolbox to create 3D volumes based on series of 2D vector drawings.
These volumes are thought to be used as input geometries for thermomechanical deformation models (e.g. Stokes flow)

In numerical models material properties can be defined (1) on elements (e.g. in body-fitted finite element), or (2) on a set of Lagrangian markers (Eulerian, ALE or mesh-free methods). Other methods such as level sets exist but are not discussed here. The purpose of geomIO is to painlessly convert drawings into material properties (1) by creating a mesh that fits the contours of the shapes drawn or by (2) assigning material phases to markers inside the shapes drawn. Users can draw a series of 2D drawings that represent 2D cross sections in the 3D space. geomIO is then used to create 3D volumes by interpolating between the cross sections. Material properties can the be assigned to markers inside the volumes. Body-fitted mesh in 3D are not supported yet.

[![](https://bitbucket.org/geomio/geomio_basics/raw/05f9a13b0eb085254a8c13c5a4e2839ae7f1bbca/icon_small.png)](https://geomio-basics.netlify.com/)


### How to use

[Full documentation is available here](https://bitbucket.org/geomio/geomio/wiki/Home)


### Getting started

1. Clone this repository, either through SSH, i.e. `git clone git@bitbucket.org:geomio/geomio.git`, or through HTTPS `git clone https://bitbucket.org/geomio/geomio.git`.
2. Change to the geomIO directory using the MATLAB console and type `install_geomIO`
3. Add the provided expression to the [startup.m](https://de.mathworks.com/help/matlab/matlab_env/add-folders-to-matlab-search-path-at-startup.html) file of your MATLAB installation.
4. In case the MATLAB startup folder and `startup.m` do not exist, you may find more information [here](https://de.mathworks.com/help/matlab/matlab_env/matlab-startup-folder.html#bvkqb5q).

```matlab
% startup.m (unix-version)


% geomIO installation
addpath(genpath('/FULL/PATH/TO/GEOMIO/INSTALLATION/geomio/src'));
```


### How to cite


Bauville, A., and T.S. Baumann. (2019), geomIO: an open‐source MATLAB toolbox to create the initial configuration of 2D/3D thermo‐mechanical simulations from 2D vector drawings, Geochem. Geophys. Geosyst., 20. [https://doi.org/10.1029/2018GC008057](https://doi.org/10.1029/2018GC008057).



``` bibtex
@article{doi:10.1029/2018GC008057,
author = {Bauville, A. and Baumann, T. S.},
title = {geomIO: an open-source MATLAB toolbox to create the initial configuration of 2D/3D thermo-mechanical simulations from 2D vector drawings},
journal = {Geochemistry, Geophysics, Geosystems},
volume = {0},
number = {ja},
pages = {},
keywords = {3D modeling, numerical simulation, MATLAB toolbox, 3D temperature distribution},
doi = {10.1029/2018GC008057},
url = {https://agupubs.onlinelibrary.wiley.com/doi/abs/10.1029/2018GC008057},
eprint = {https://agupubs.onlinelibrary.wiley.com/doi/pdf/10.1029/2018GC008057}
}
```

### License

    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
    All rights reserved.

    geomIO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, version 3 of the License.

    geomIO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with geomIO. If not, see <http://www.gnu.org/licenses/>.

[Full license is available here](./LICENSE.txt)


### Acknowledgements 
geomIO's development has been supported by the European Research Council under the European Community's Seventh Framework program (FP7/2007/2013) with ERC starting grant agreement no.258830.

![](/doc/ERC_logo.jpg)

![](/doc/2000px-Johannes_Gutenberg-Universitat_Mainz_logo.svg.png)
